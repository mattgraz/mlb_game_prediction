from setuptools import setup, find_packages

setup(name='pymlb',
      version='0.1',
      description='MLB data pipeline and models',
      url='https://gitlab.com/mattgraz/mlb_game_prediction',
      author='Graz',
      author_email='matthew.graziano1@gmail.com',
      license='MIT',
      packages=find_packages())