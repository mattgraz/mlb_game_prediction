
SELECT DISTINCT
    p.date,
    p.home_team,
    p.away_team,
    p.inning_num,
    p.bat_mlbid,
    p.pit_mlbid,
    p.bat_hand,
    p.pit_hand,
    p.at_bat_outcome,
    at_bat_outcome_description,
    p.at_bat_pitch_counter,
    p.pitch_ax,
    p.pitch_ay,
    p.pitch_az,
    p.pitch_break_angle,
    p.pitch_break_length,
    p.pitch_break_y,
    p.pitch_type,
    p.pitch_type_confidence,
    p.pitch_nasty_factor,
    p.pitch_spin_dir,
    p.pitch_spin_rate,
    p.pitch_start_speed,
    p.pitch_end_speed,
    p.pitch_pfx_x,
    p.pitch_pfx_z,
    p.pitch_px,
    p.pitch_pz,
    p.pitch_vx0,
    p.pitch_vy0,
    p.pitch_vz0,
    p.pitch_x,
    p.pitch_x0,
    p.pitch_y,
    p.pitch_y0,
    p.pitch_z0,
    p.pitch_outcome_description
From PitchFX p

left join game_outcomes as g
on p.home_team = g.home_code and
   p.away_team = g.away_code and
   p.date = g.game_date

WHERE
    g.game_type NOT IN ('E', 'S', 'A') and
    date >= "{}" AND
    date <= "{}"

ORDER BY
    date,
    home_team,
    away_team,
    inning_num,
    at_bat_pitch_counter
