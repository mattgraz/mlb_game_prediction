SELECT
    Date,
    HomeStartingPitcherID,
    VisitorStartingPitcherID
FROM
    gamelogs

WHERE
    Date < "{}" AND
    (HomeStartingPitcherID IN ({}) OR
    VisitorStartingPitcherID IN ({}))
ORDER BY
    Date DESC

limit {}


