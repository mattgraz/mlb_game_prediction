Select
    pit_mlbid,
    pitch_type,
    count(*) pitch_count

from
    "pitchFx" as p

left join game_outcomes as g
on p.home_team = g.home_code and
   p.away_team = g.away_code and
   p.date = g.game_date

where
    pit_mlbid in ({}) and
    bat_hand in ({}) and
    date >= '{}' and
    date <= '{}' and
    -- Remove Spring Training and other Exhibition games
    g.game_type NOT IN ('E', 'S', 'A') and
    pitch_type NOT IN ('UN', 'FO', 'PO', 'AB', 'IN', 'AS', '')

group by
    pit_mlbid,
    pitch_type
