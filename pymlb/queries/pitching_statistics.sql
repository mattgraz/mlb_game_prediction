Select distinct
    p.date,
    home_team,
    away_team,
    inning_num,
    bat_mlbid,
    pit_mlbid,
    bat_hand,
    pit_hand,
    at_bat_outcome,
    at_bat_outcome_description
from
    "pitchFx" as p

left join game_outcomes as g
on p.home_team = g.home_code and
   p.away_team = g.away_code and
   p.date = g.game_date

where
    pit_mlbid in ({}) and
    bat_hand in ({}) and
    date >= '{}' and
    date <= '{}' and
    -- Remove Spring Training and other Exhibition games
    g.game_type NOT IN ('E', 'S', 'A')
