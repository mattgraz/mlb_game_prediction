Select
    *,
    g.game_type
from
    "pitchFx" as p

left join game_outcomes as g
on p.home_team = g.home_code and
   p.away_team = g.away_code and
   p.date = g.game_date

where
    date <= '{}' and
    bat_mlbid = '{}' and
    -- Remove Spring Training and other Exhibition games
    g.game_type NOT IN ('E', 'S', 'A') and
    pitch_type NOT IN ('UN', 'FO', 'PO', 'AB', 'IN', 'AS')
