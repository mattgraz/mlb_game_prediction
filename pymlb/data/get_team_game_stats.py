import pandas as pd
import numpy as np
from pymlb.util.database import database_mlb_connection, read_sql
from pymlb.util.feature import team_batting_features, pitching_features
from pymlb.util.data import get_game_logs, get_player_id_mapping
from pymlb.util.utils import get_config
import concurrent.futures

from tqdm import tqdm


# I need statistics per game and I need to aggregate those game by game
def get_game_features(
    game, batter_feature_timeframe="current_season", pitcher_stats_n_starts=5
):
    """
        Summary:
            Calculates batter and pitcher statistics for some specified time period at the time of the input game

        game (tuple): A row from get_game_logs().iterrows()

        batter_feature_timeframe (str): Defines the timeframe batter statistics will be calculated for
            'current_season': Calculates all batter statistics using the current season
            'career': Will use all data available up to a game date to calculate statistics
            't10': trailing 10 games
            't30': trailing 30 games

        pitcher_stats_n_starts (int): Defines the number of trailing starts pitcher statistics are calculated for

    """

    home_batter_col_names = [
        "homeBatting1PlayerID",
        "homeBatting2PlayerID",
        "homeBatting3PlayerID",
        "homeBatting4PlayerID",
        "homeBatting5PlayerID",
        "homeBatting6PlayerID",
        "homeBatting7PlayerID",
        "homeBatting8PlayerID",
        "homeBatting9PlayerID",
    ]

    away_batter_col_names = [
        "visitorBatting1PlayerID",
        "visitorBatting2PlayerID",
        "visitorBatting3PlayerID",
        "visitorBatting4PlayerID",
        "visitorBatting5PlayerID",
        "visitorBatting6PlayerID",
        "visitorBatting7PlayerID",
        "visitorBatting8PlayerID",
        "visitorBatting9PlayerID",
    ]

    home_batters_ids_list = game[1][home_batter_col_names].values
    away_batters_ids_list = game[1][away_batter_col_names].values

    # Determine handedness of each of the home/away starting pitchers
    home_pitch_id_str = game[1]["homeStartingPitcherID"]
    away_pitch_id_str = game[1]["visitorStartingPitcherID"]
    home_pitcher_hand_str = pd.read_sql(
        f'select distinct pit_hand from "pitchFx" where pit_mlbid = "{home_pitch_id_str}" limit 1',
        con,
    ).pit_hand.values[0]
    away_pitcher_hand_str = pd.read_sql(
        f'select distinct pit_hand from "pitchFx" where pit_mlbid = "{away_pitch_id_str}" limit 1',
        con,
    ).pit_hand.values[0]

    # determine start date based on batter_feature_timeframe
    if batter_feature_timeframe == "current_season":
        start_date = "{game[1]['date'].year}-01-01"

    elif batter_feature_timeframe == "career":
        start_date = "2008-01-01"

    elif batter_feature_timeframe == "t10":
        start_date = game[1]["date"] - pd.Timedelta(days=10)
        start_date = f"{game[1]['date'].year}-01-01"

    elif batter_feature_timeframe == "t30":
        start_date = game[1]["date"] - pd.Timedelta(days=30)
        start_date = f"{game[1]['date'].year}-01-01"

    end_date = game[1].date - pd.Timedelta(days=1)
    end_date = end_date.strftime("%Y-%m-%d")

    home_team_batting_features = team_batting_features(
        mlb_id=home_batters_ids_list,
        pit_hand=[away_pitcher_hand_str],
        start_date=start_date,
        end_date=end_date,
    )

    away_team_batting_features = team_batting_features(
        mlb_id=away_batters_ids_list,
        pit_hand=[home_pitcher_hand_str],
        start_date=start_date,
        end_date=end_date,
    )

    # Get starting pitcher's statistics

    # Get date range encompassing pitcher's last pitcher_stats_n_starts starts (or less if no data)
    pitcher_start_dates_query = read_sql("pymlb/queries/get_pitcher_last_n_starts.sql")
    away_pitcher_id_query_string = "".join(
        "'" + retro_to_mlbid_mapping_dict[str(away_pitch_id_str)] + "'"
    )
    home_pitcher_id_query_string = "".join(
        "'" + retro_to_mlbid_mapping_dict[str(home_pitch_id_str)] + "'"
    )

    away_pitcher_start_dates_query = pitcher_start_dates_query.format(
        end_date,
        away_pitcher_id_query_string,
        away_pitcher_id_query_string,
        pitcher_stats_n_starts,
    )

    home_pitcher_start_dates_query = pitcher_start_dates_query.format(
        end_date,
        home_pitcher_id_query_string,
        home_pitcher_id_query_string,
        pitcher_stats_n_starts,
    )

    away_pitching_last_n_start_dates = pd.read_sql(away_pitcher_start_dates_query, con)
    away_pitcher_start_date = pd.to_datetime(
        away_pitching_last_n_start_dates.date.min(), format="%Y%m%d"
    ).strftime("%Y-%m-%d")
    away_pitcher_end_date = pd.to_datetime(
        away_pitching_last_n_start_dates.date.max(), format="%Y%m%d"
    ).strftime("%Y-%m-%d")

    home_pitching_last_n_start_dates = pd.read_sql(home_pitcher_start_dates_query, con)
    home_pitcher_start_date = pd.to_datetime(
        home_pitching_last_n_start_dates.date.min(), format="%Y%m%d"
    ).strftime("%Y-%m-%d")
    home_pitcher_end_date = pd.to_datetime(
        home_pitching_last_n_start_dates.date.max(), format="%Y%m%d"
    ).strftime("%Y-%m-%d")

    # Get pitcher features
    away_starting_pitcher_features = pitching_features(
        mlb_id=[away_pitch_id_str],
        bat_hand=["L", "R"],
        start_date=away_pitcher_start_date,
        end_date=away_pitcher_end_date,
    )

    home_starting_pitcher_features = pitching_features(
        mlb_id=[home_pitch_id_str],
        bat_hand=["L", "R"],
        start_date=home_pitcher_start_date,
        end_date=home_pitcher_end_date,
    )

    # Combine the above features together
    # NOTE: Away/Home are considered separate rows in this dataset as we are attempting to predict the number of runs scored by a team

    # Create new rows
    home_team_row = pd.concat(
        [home_team_batting_features, away_starting_pitcher_features], axis=1
    )
    away_team_row = pd.concat(
        [away_team_batting_features, home_starting_pitcher_features], axis=1
    )

    # Add metadata to datasets
    # Date
    home_team_row["game_date"] = game[1]["date"]
    away_team_row["game_date"] = game[1]["date"]

    # Team
    home_team_row["team"] = game[1]["HomeTeam"]
    away_team_row["team"] = game[1]["VisitingTeam"]

    # League Rules (home team)
    home_team_row["league_rules"] = game[1]["HomeTeamLeague"]
    away_team_row["league_rules"] = game[1]["HomeTeamLeague"]

    # Game number for each team
    home_team_row["game_number"] = game[1]["HomeTeamGameNumber"]
    away_team_row["game_number"] = game[1]["VisitingTeamGameNumber"]

    # Park ID
    home_team_row["parkID"] = game[1]["ParkID"]
    away_team_row["parkID"] = game[1]["ParkID"]

    # Home plate umpire
    home_team_row["home_plate_umpire"] = game[1]["UmpireHID"]
    away_team_row["home_plate_umpire"] = game[1]["UmpireHID"]

    # Game length (outs)
    home_team_row["game_outs"] = game[1]["LengthInOuts"]
    away_team_row["game_outs"] = game[1]["LengthInOuts"]

    # day or night game
    home_team_row["day_or_night"] = game[1]["DayNight"]
    away_team_row["day_or_night"] = game[1]["DayNight"]

    # Pull the response variable (number of runs scored) and place it at the s
    home_team_row["runs"] = game[1]["HomeRunsScore"]
    away_team_row["runs"] = game[1]["VisitorRunsScored"]

    # Add key to datasets (date, home team, away team, VisitingTeamGameNumber, HomeTeamGameNum
    # NOTE The game number is included to handle double headers)
    key = (
        str(game[1]["date"])[0:10]
        + "_"
        + str(game[1]["HomeTeam"])
        + "_"
        + str(game[1]["VisitingTeam"])
        + "_"
        + str(game[1]["HomeTeamGameNumber"])
        + "_"
        + str(game[1]["VisitingTeamGameNumber"])
    )

    home_team_row["gamekey"] = key
    away_team_row["gamekey"] = key

    out_rows = pd.concat([home_team_row, away_team_row], axis=0, ignore_index=True)
    return out_rows


if __name__ == "__main__":

    config = get_config()

    con = database_mlb_connection()

    training_years = list(
        range(
            config["data"]["train"]["start_year"], config["data"]["train"]["end_year"]
        )
    )
    testing_years = list(
        range(config["data"]["test"]["start_year"], config["data"]["test"]["end_year"])
    )
    min_year = np.min(training_years + testing_years)
    max_year = np.max(training_years + testing_years)

    # Get Game Logs
    gameLogs = get_game_logs(f"{min_year}-01-01", f"{max_year}-12-31")

    # Certain games should be removed from the training set such as:
    #   - Games less than 9 innings
    #   - Exhibition games
    #   - Extra Inning games:
    #       Since my analysis primarily focuses on impacts of starting pitcher, closer, etc. does it make sense
    #       to include games where other pitchers probably have significant innings logged

    # Create dictionary that contains mapping of retro ID to mlb ID and vice versa
    mlbid_to_retro_mapping_dict = get_player_id_mapping("key_retro", "key_mlbam")
    retro_to_mlbid_mapping_dict = get_player_id_mapping("key_mlbam", "key_retro")

    # For all playerID columns in gameLogs, I need to look up the player's mlbID and replace the retrosheetid
    for col in gameLogs.columns:
        if ("PlayerID" in col) | ("PitcherID" in col):
            print(col)
            col_mlb_id_series = [
                int(mlbid_to_retro_mapping_dict.get(retroID, -99999))
                for retroID in gameLogs[col]
            ]
            gameLogs[col] = col_mlb_id_series

    all_games_list = []
    with concurrent.futures.ProcessPoolExecutor(8) as pool:
        results = [
            pool.submit(
                get_game_features,
                game=game,
                batter_feature_timeframe=config["data"]["batter_feature_timeframe"],
            )
            for game in list(gameLogs.iterrows())
        ]
        done_results = concurrent.futures.as_completed(results)

        for _ in tqdm(results):
            # Get results of cut and add to cached_ci, a dataframe containing all results
            output = next(done_results).result()
            all_games_list.append(output)

    all_games_df = pd.concat(all_games_list)

    all_games_df.to_parquet("data/team_game_stats_clean.pq")
