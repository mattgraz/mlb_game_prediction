"""
Stores player id data to sqlite database
"""

import sqlite3
import pandas as pd

from pymlb.util.utils import get_config

from pymlb.util.database import database_mlb_engine

config = get_config()
engine = database_mlb_engine()

# Read player id data
player_id_url = config["data"]["download_urls"]["player_id_map"]
player_id_data = pd.read_csv(player_id_url, low_memory=False)

# keep only relevant player id columns in database
player_id_data = player_id_data[
    ["key_mlbam", "key_retro", "key_bbref", "key_bbref_minors", "key_fangraphs"]
].copy()

# drop any data where all cols are na
player_id_data.dropna(how="all", inplace=True)
player_id_data.reset_index(drop=True, inplace=True)

player_id_data.to_sql(
    name="player_id_mapping", con=engine, if_exists="replace", index=False
)
