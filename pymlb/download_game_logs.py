""" Purpose: Downloads Retrosheet game by game data """

from urllib.request import urlopen
import os
import zipfile
import json
import pandas as pd
from tqdm import tqdm

from pymlb.util.database import database_mlb_engine

engine = database_mlb_engine()


def download_game_logs(outdir, years=range(2006, 2016)):
    """
    Purpose: Downloads game by game .csv database and moves files into appropriate directory
    url: String of URL of .zip file to download
    outdir: String of output directory
    """

    # Check if data/external exists and create if it does not
    try:
        os.stat(outdir)
    except FileNotFoundError:
        os.makedirs(outdir)

    # Iterate through each desired year and download game logs
    for year in tqdm(years):
        try:
            file_url = urlopen(
                "http://www.retrosheet.org/gamelogs/gl{}.zip".format(year)
            )

            # Download and write file
            with open(outdir + "/gamelogs_{}.zip".format(year), "wb") as file:
                file.write(file_url.read())

            # Unzip file
            with zipfile.ZipFile(outdir + "/gamelogs_{}.zip".format(year)) as zip_file:
                zip_file.extractall(outdir)
        except:
            print("Data for {} unavailable".format(year))
            continue
    return 0


# Define column names for game log datasets
# Column names courtasy of: \
#   https://raw.githubusercontent.com/maxtoki/baseball_R/master/data/game_log_header.csv
# Column Definitions: http://www.retrosheet.org/gamelogs/glfields.txt
FIELDS = [
    "Date",
    "DoubleHeader",
    "DayOfWeek",
    "VisitingTeam",
    "VisitingTeamLeague",
    "VisitingTeamGameNumber",
    "HomeTeam",
    "HomeTeamLeague",
    "HomeTeamGameNumber",
    "VisitorRunsScored",
    "HomeRunsScore",
    "LengthInOuts",
    "DayNight",
    "CompletionInfo",
    "ForfeitInfo",
    "ProtestInfo",
    "ParkID",
    "Attendence",
    "Duration",
    "VisitorLineScore",
    "HomeLineScore",
    "VisitorAB",
    "VisitorH",
    "VisitorD",
    "VisitorT",
    "VisitorHR",
    "VisitorRBI",
    "VisitorSH",
    "VisitorSF",
    "VisitorHBP",
    "VisitorBB",
    "VisitorIBB",
    "VisitorK",
    "VisitorSB",
    "VisitorCS",
    "VisitorGDP",
    "VisitorCI",
    "VisitorLOB",
    "VisitorPitchers",
    "VisitorER",
    "VisitorTER",
    "VisitorWP",
    "VisitorBalks",
    "VisitorPO",
    "VisitorA",
    "VisitorE",
    "VisitorPassed",
    "VisitorDB",
    "VisitorTP",
    "HomeAB",
    "HomeH",
    "HomeD",
    "HomeT",
    "HomeHR",
    "HomeRBI",
    "HomeSH",
    "HomeSF",
    "HomeHBP",
    "HomeBB",
    "HomeIBB",
    "HomeK",
    "HomeSB",
    "HomeCS",
    "HomeGDP",
    "HomeCI",
    "HomeLOB",
    "HomePitchers",
    "HomeER",
    "HomeTER",
    "HomeWP",
    "HomeBalks",
    "HomePO",
    "HomeA",
    "HomeE",
    "HomePassed",
    "HomeDB",
    "HomeTP",
    "UmpireHID",
    "UmpireHName",
    "Umpire1BID",
    "Umpire1BName",
    "Umpire2BID",
    "Umpire2BName",
    "Umpire3BID",
    "Umpire3BName",
    "UmpireLFID",
    "UmpireLFName",
    "UmpireRFID",
    "UmpireRFName",
    "VisitorManagerID",
    "VisitorManagerName",
    "HomeManagerID",
    "HomeManagerName",
    "WinningPitcherID",
    "WinningPitcherName",
    "LosingPitcherID",
    "LosingPitcherNAme",
    "SavingPitcherID",
    "SavingPitcherName",
    "GameWinningRBIID",
    "GameWinningRBIName",
    "VisitorStartingPitcherID",
    "VisitorStartingPitcherName",
    "HomeStartingPitcherID",
    "HomeStartingPitcherName",
    "VisitorBatting1PlayerID",
    "VisitorBatting1Name",
    "VisitorBatting1Position",
    "VisitorBatting2PlayerID",
    "VisitorBatting2Name",
    "VisitorBatting2Position",
    "VisitorBatting3PlayerID",
    "VisitorBatting3Name",
    "VisitorBatting3Position",
    "VisitorBatting4PlayerID",
    "VisitorBatting4Name",
    "VisitorBatting4Position",
    "VisitorBatting5PlayerID",
    "VisitorBatting5Name",
    "VisitorBatting5Position",
    "VisitorBatting6PlayerID",
    "VisitorBatting6Name",
    "VisitorBatting6Position",
    "VisitorBatting7PlayerID",
    "VisitorBatting7Name",
    "VisitorBatting7Position",
    "VisitorBatting8PlayerID",
    "VisitorBatting8Name",
    "VisitorBatting8Position",
    "VisitorBatting9PlayerID",
    "VisitorBatting9Name",
    "VisitorBatting9Position",
    "HomeBatting1PlayerID",
    "HomeBatting1Name",
    "HomeBatting1Position",
    "HomeBatting2PlayerID",
    "HomeBatting2Name",
    "HomeBatting2Position",
    "HomeBatting3PlayerID",
    "HomeBatting3Name",
    "HomeBatting3Position",
    "HomeBatting4PlayerID",
    "HomeBatting4Name",
    "HomeBatting4Position",
    "HomeBatting5PlayerID",
    "HomeBatting5Name",
    "HomeBatting5Position",
    "HomeBatting6PlayerID",
    "HomeBatting6Name",
    "HomeBatting6Position",
    "HomeBatting7PlayerID",
    "HomeBatting7Name",
    "HomeBatting7Position",
    "HomeBatting8PlayerID",
    "HomeBatting8Name",
    "HomeBatting8Position",
    "HomeBatting9PlayerID",
    "HomeBatting9Name",
    "HomeBatting9Position",
    "AdditionalInfo",
    "AcquisitionInfo",
]

if __name__ == "__main__":
    with open("update_dates_dict.txt", "r") as f:
        update_dates_dict = json.load(f)

    start_year = pd.to_datetime(update_dates_dict["UPDATE_START_DATE_GAMELOGS"]).year
    end_year = pd.to_datetime(update_dates_dict["UPDATE_END_DATE_GAMELOGS"]).year

    # download game logs data
    YEARS = range(start_year, end_year + 1)
    RET_CODE = download_game_logs("data/external/gameLogs", years=YEARS)

    # load each year downloaded, rbind rows, output as one dataset
    gamelogs = []
    for year in tqdm(YEARS):
        try:
            glYear = pd.read_csv(
                "data/external/gamelogs/GL{}.TXT".format(year), names=FIELDS
            )
            gamelogs.append(glYear)
        except FileNotFoundError:
            continue

    gamelogs = pd.concat(gamelogs)

    # Make first letter lowercase so names are in camelCase
    gamelogs.columns = [col[0].lower() + col[1:] for col in gamelogs.columns]

    # Convert date to YYYY-MM-DD format (datetime to string)
    gamelogs["date"] = pd.to_datetime(gamelogs.date, format="%Y%m%d").dt.date

    # Make sure duplicate data not getting uploaded
    gamelogs = gamelogs[
        gamelogs.date
        > pd.to_datetime(update_dates_dict["UPDATE_START_DATE_GAMELOGS"]).date()
    ]

    gamelogs.to_sql(name="gamelogs", con=engine, if_exists="replace", index=False)
