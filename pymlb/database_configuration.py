import sqlite3

# Attempt database connection
try:
    con = sqlite3.connect("data/mlb.db")
except:
    print("Database connection cannot be established")

# Define cursor to work with
cur = con.cursor()

# Create plyaerID table
cur.execute(
    "CREATE TABLE IF NOT EXISTS player_id \
                (mlb_id integer PRIMARY KEY,\
                mlb_name varchar(18),\
                mlb_pos varchar(2),\
                mlb_team varchar(3),\
                mlb_team_long varchar(21),\
                bats varchar(1),\
                throws varchar(1),\
                birth_year smallInt,\
                bp_id integer,\
                bref_id varchar(9),\
                bref_name varchar(18),\
                cbs_id integer,\
                cbs_name varchar(18),\
                cbs_pos varchar(2),\
                espn_id integer,\
                espn_name varchar(18),\
                espn_pos varchar(2),\
                fg_id integer,\
                fg_name varchar(18),\
                fg_pos varchar(2),\
                lahman_id varchar(9),\
                nfbc_id integer,\
                nfbc_name varchar(18),\
                nfbc_pos varchar(2),\
                retro_id varchar(8),\
                retro_name varchar(18),\
                debut date,\
                yahoo_id integer,\
                yahoo_name varchar(18),\
                yahoo_pos varchar(2),\
                mlb_depth varchar(4),\
                ottoneu_id integer,\
                ottoneu_name varchar(18),\
                ottoneu_pos varchar(2))"
)


# Create batter outcome type counts
cur.execute(
    "CREATE TABLE IF NOT EXISTS batter_outcome_type_counts\
                (data_through_date date NOT NULL,\
                bat_mlbid integer NOT NULL,\
                pit_hand_cd varchar(1) NOT NULL,\
                bat_hand_cd varchar(1) NOT NULL,\
                error smallInt,\
                hit_bunt smallInt,\
                hit_fly smallInt,\
                hit_ground smallInt,\
                hit_line smallInt,\
                hit_unknown smallInt,\
                out_fly smallInt,\
                out_ground smallInt,\
                out_line smallInt,\
                out_unknown smallInt,\
                strikeout smallInt,\
                walk smallInt,\
                PRIMARY KEY (data_through_date, bat_mlbid, pit_hand_cd, bat_hand_cd))"
)

# Create batter outcome type rates
cur.execute(
    "CREATE TABLE IF NOT EXISTS batter_outcome_type_rates\
                (data_through_date date NOT NULL,\
                bat_mlbid integer NOT NULL,\
                pit_hand_cd varchar(1) NOT NULL,\
                bat_hand_cd varchar(1) NOT NULL,\
                error smallInt,\
                hit_bunt smallInt,\
                hit_fly smallInt,\
                hit_ground smallInt,\
                hit_line smallInt,\
                hit_unknown smallInt,\
                out_fly smallInt,\
                out_ground smallInt,\
                out_line smallInt,\
                out_unknown smallInt,\
                strikeout smallInt,\
                walk smallInt,\
                PRIMARY KEY (data_through_date, bat_mlbid, pit_hand_cd, bat_hand_cd))"
)

cur.execute(
    "CREATE TABLE IF NOT EXISTS pitch_type_counts\
                (data_through_date date NOT NULL,\
                pit_id integer NOT NULL,\
                pit_hand_cd varchar(1),\
                bat_hand_cd varchar(1),\
                game_id_count integer,\
                CH real,\
                CU real,\
                EP real,\
                FA real,\
                FC real,\
                FF real,\
                FS real,\
                FT real,\
                KC real,\
                KN real,\
                SC real,\
                SI real,\
                SL real,\
                PRIMARY KEY (data_through_date, pit_id))"
)

cur.execute(
    "CREATE TABLE IF NOT EXISTS pitch_type_rates\
                (data_through_date date NOT NULL,\
                pit_id integer NOT NULL,\
                pit_hand_cd varchar(1),\
                bat_hand_cd varchar(1),\
                game_id_count integer,\
                CH real,\
                CU real,\
                EP real,\
                FA real,\
                FC real,\
                FF real,\
                FS real,\
                FT real,\
                KC real,\
                KN real,\
                SC real,\
                SI real,\
                SL real,\
                PRIMARY KEY (data_through_date, pit_id))"
)

cur.execute(
    """
    CREATE TABLE IF NOT EXISTS "pitchFx" (
        date DATE,
        home_team TEXT,
        away_team TEXT,
        inning_num BIGINT,
        bat_mlbid TEXT,
        pit_mlbid TEXT,
        bat_hand TEXT,
        pit_hand TEXT,
        away_team_runs BIGINT,
        home_team_runs BIGINT,
        at_bat_outcome TEXT,
        at_bat_outcome_description TEXT,
        at_bat_pitch_counter BIGINT,
        pitch_ax FLOAT,
        pitch_ay FLOAT,
        pitch_az FLOAT,
        pitch_break_angle FLOAT,
        pitch_break_length FLOAT,
        pitch_break_y FLOAT,
        pitch_batting_count_type TEXT,
        pitch_type TEXT,
        pitch_type_confidence FLOAT,
        pitch_outcome_description TEXT,
        pitch_nasty_factor FLOAT,
        pitch_pfx_x FLOAT,
        pitch_pfx_z FLOAT,
        pitch_px FLOAT,
        pitch_pz FLOAT,
        pitch_spin_dir FLOAT,
        pitch_spin_rate FLOAT,
        pitch_start_speed FLOAT,
        pitch_end_speed FLOAT,
        pitch_vx0 FLOAT,
        pitch_vy0 FLOAT,
        pitch_vz0 FLOAT,
        pitch_x FLOAT,
        pitch_x0 FLOAT,
        pitch_y FLOAT,
        pitch_y0 FLOAT,
        pitch_z0 FLOAT,
        pitch_zone FLOAT,
        pitch_time_zulu TEXT )
"""
)

con.close()
