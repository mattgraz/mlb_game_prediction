"""
Purpose: Queries database tables for data date range and downloads new data
for relevant tables
"""

import os
import pandas as pd
from datetime import date, timedelta, datetime
import json

# Import Database Connection
from pymlb.util.database import database_mlb_cursor, read_sql

cursor = database_mlb_cursor()

update_dates_dict = {}

yesterday = date.today() - timedelta(days=1)

#################
# Pitch FX Data #
#################

# Import query to check pitch_type counts table
# This is a check for pitch fx data pull
query = read_sql("pymlb/queries/pitcher_game_data_date_range_check.sql")

try:
    # execute our Query
    cursor.execute(query)
    min_date, max_date = cursor.fetchall()[0]

    # update_date = pd.to_datetime(max_date) - timedelta(days=1)
    update_date = pd.to_datetime(max_date)

except:
    print("No PitchFx data exists. Defaulting ot 2008-01-15")
    update_date = datetime(2008, 1, 1)

update_dates_dict["UPDATE_START_DATE_PITCHFX"] = "{}-{}-{}".format(
    str(update_date.year),
    str(update_date.month).zfill(2),
    str(update_date.day).zfill(2),
)
update_dates_dict["UPDATE_END_DATE_PITCHFX"] = "{}-{}-{}".format(
    str(yesterday.year), str(yesterday.month).zfill(2), str(yesterday.day).zfill(2)
)

#################
# Game Outcomes #
#################
query = read_sql("pymlb/queries/game_outcomes_date_range_check.sql")

try:
    # execute our Query
    cursor.execute(query)
    min_date, max_date = cursor.fetchall()[0]

    # update_date = pd.to_datetime(max_date) - timedelta(days=1)
    update_date = pd.to_datetime(max_date)

except:
    print("No Game Outcomes data exists. Defaulting ot 2008-01-15")
    update_date = datetime(2008, 1, 1)

update_dates_dict["UPDATE_START_DATE_GAME_OUTCOMES"] = "{}-{}-{}".format(
    str(update_date.year),
    str(update_date.month).zfill(2),
    str(update_date.day).zfill(2),
)
update_dates_dict["UPDATE_END_DATE_GAME_OUTCOMES"] = "{}-{}-{}".format(
    str(yesterday.year), str(yesterday.month).zfill(2), str(yesterday.day).zfill(2)
)

#############
# GAME LOGS #
#############
query = read_sql("pymlb/queries/game_logs_date_range_check.sql")

try:
    # execute our Query
    cursor.execute(query)
    min_date, max_date = cursor.fetchall()[0]

    # update_date = pd.to_datetime(max_date, format="%Y%m%d") - timedelta(days=1)
    update_date = pd.to_datetime(max_date)
except:
    print("No Game Log data exists. Defaulting ot 2008-01-15")
    update_date = datetime(2008, 1, 1)

update_dates_dict["UPDATE_START_DATE_GAMELOGS"] = "{}-{}-{}".format(
    str(update_date.year),
    str(update_date.month).zfill(2),
    str(update_date.day).zfill(2),
)
update_dates_dict["UPDATE_END_DATE_GAMELOGS"] = "{}-{}-{}".format(
    str(yesterday.year), str(yesterday.month).zfill(2), str(yesterday.day).zfill(2)
)

with open("update_dates_dict.txt", "w") as output:
    json.dump(update_dates_dict, output)
