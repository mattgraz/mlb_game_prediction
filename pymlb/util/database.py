import sqlite3
from sqlalchemy import create_engine

database_relative_path = "data/mlb.db"


def database_mlb_connection():
    conn = sqlite3.connect(database_relative_path)
    return conn


def database_mlb_engine():
    engine = create_engine("sqlite:///" + database_relative_path)
    return engine


def database_mlb_cursor():
    conn = database_mlb_connection()
    cursor = conn.cursor()
    return cursor


def read_sql(filepath):
    file = open(filepath, "r")
    query_string = file.read()
    file.close
    return query_string
