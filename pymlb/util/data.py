
import pandas as pd
from pymlb.util.database import database_mlb_connection, read_sql


def get_game_logs(start_date, end_date):
    # Summary: Pulls mlb game logs between some start/end dates

    con = database_mlb_connection()

    game_log_query = read_sql("pymlb/queries/get_gamelog.sql")

    # Add start/end date to query
    game_log_query = game_log_query.format(start_date, end_date)

    gameLogs = pd.read_sql(game_log_query, con)
    con.close()

    gameLogs["date"] = pd.to_datetime(gameLogs["date"]).dt.date
    gameLogs = gameLogs.sort_values("date")

    return gameLogs


def get_player_id_mapping(key, value):
    # returns a mapping dictionary for one playerID schema to another
    # key/value (str): one of key_mlbam, "key_retro", "key_bbref", "key_bbref_minors", "key_fangraphs",

    con = database_mlb_connection()

    player_id_mapping_query = read_sql("pymlb/queries/get_player_id_mapping.sql")
    player_id_mapping = pd.read_sql(player_id_mapping_query, con)
    con.close()

    # Filter to only key/value columns
    player_id_mapping = player_id_mapping[[key, value]]

    # Drop any rows where all ids are nan
    player_id_mapping.dropna(how="all", inplace=True)

    # Drop any rows where Key is nan
    player_id_mapping = player_id_mapping[~player_id_mapping[key].isna()]

    # Remove any odd encodings
    player_id_mapping[key] = player_id_mapping[key].astype(str).str.replace("\.0", "")
    player_id_mapping[value] = (
        player_id_mapping[value].astype(str).str.replace("\.0", "")
    )

    # Create mapping dictionary of key to value
    mapping = dict(zip(player_id_mapping[key], player_id_mapping[value]))

    return mapping


def get_pitches(start_date, end_date):
    """
    Summary:
        Returns all non-exhibition game pitches thrown during a specified timeframe

        start_date / end_date (str): A date of format ("YYYY-MM-DD")

    returns:
        pd.DataFrame containing pitch metadata

    """

    def map_outcomes_to_general_contact_types(df):
        """
        Takes as input a dataframe with a column at_bat_outcome. Said column will be remapped to contact type events (line, ground, fly, bunt, non-contact) called general_contact_types
        and a pd.DataFrame will be returned

        Input:
            df (pd.DataFrame)

        Output:
            df (pd.DataFrame)

        """

        general_contact_type_mapping = {
            "batter interference": "non_contact",
            "bunt ground Out": "bunt",
            "bunt groundout": "bunt",
            "bunt lineout": "bunt",
            "bunt pop out": "bunt",
            "catcher interference": "non_contact",
            "double": "double",
            "double play": "double play",
            "fan interference": "non_contact",
            "field error": "error",
            "fielders choice": "ground",
            "fielders choice out": "ground",
            "fly out": "fly",
            "flyout": "fly",
            "force out": "forceout",
            "forceout": "forceout",
            "ground out": "ground",
            "grounded into dp": "ground",
            "groundout": "ground",
            "hit by pitch": "non_contact",
            "home run": "home run",
            "intent walk": "non_contact",
            "line out": "line",
            "lineout": "line",
            "pop out": "fly",
            "runner out": "non_contact",
            "sac bunt": "bunt",
            "sac fly": "fly",
            "sac fly dp": "fly",
            "sacrifice bunt dp": "bunt",
            "single": "single",
            "strikeout": "non_contact",
            "strikeout - dp": "non_contact",
            "triple": "triple",
            "triple play": "triple play",
            "walk": "non_contact",
        }

        df["general_contact_types"] = df.at_bat_outcome.str.lower()

        # Apply new labels
        df.general_contact_types = df.general_contact_types.map(
            general_contact_type_mapping
        )

        # Replace NaN values with at_bat_outcome
        df.general_contact_types.fillna(df.at_bat_outcome, inplace=True)

        # Check where general labels can be made more specific and adjust label to groundout, flyout, or lineout appropriately
        df.loc[
            (df.general_contact_types == "forceout")
            & (df.at_bat_outcome_description.str.contains(" ground")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "forceout")
            & (df.at_bat_outcome_description.str.contains(" flies")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "forceout")
            & (df.at_bat_outcome_description.str.contains(" pops")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "forceout")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "forceout")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"

        df.loc[
            (df.general_contact_types == "double play")
            & (df.at_bat_outcome_description.str.contains(" ground")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "double play")
            & (df.at_bat_outcome_description.str.contains(" flies")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "double play")
            & (df.at_bat_outcome_description.str.contains(" pops")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "double play")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "double play")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"

        df.loc[
            (df.general_contact_types == "triple play")
            & (df.at_bat_outcome_description.str.contains(" ground")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "triple play")
            & (df.at_bat_outcome_description.str.contains(" flies")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "triple play")
            & (df.at_bat_outcome_description.str.contains(" pops")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "triple play")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "triple play")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"

        # NOTE: There are a small number of hits that were also double plays that are counted as "hit-Out in this ins"

        # Categorize hits as linedrive hits, groundball hits, or flyball hits
        df.loc[
            (df.general_contact_types == "single")
            & (df.at_bat_outcome_description.str.contains(" bunt")),
            "general_contact_types",
        ] = "bunt"
        df.loc[
            (df.general_contact_types == "single")
            & (df.at_bat_outcome_description.str.contains(" ground ball")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "single")
            & (df.at_bat_outcome_description.str.contains(" fly ball")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "single")
            & (df.at_bat_outcome_description.str.contains(" pop up")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "single")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "single")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"
        df.loc[df.general_contact_types == "single", "general_contact_types"] = "ground"

        df.loc[
            (df.general_contact_types == "double")
            & (df.at_bat_outcome_description.str.contains(" bunt")),
            "general_contact_types",
        ] = "bunt"
        df.loc[
            (df.general_contact_types == "double")
            & (df.at_bat_outcome_description.str.contains(" ground ball")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "double")
            & (df.at_bat_outcome_description.str.contains(" fly ball")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "double")
            & (df.at_bat_outcome_description.str.contains(" pop up")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "double")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "double")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"
        df.loc[df.general_contact_types == "double", "general_contact_types"] = "line"

        df.loc[
            (df.general_contact_types == "triple")
            & (df.at_bat_outcome_description.str.contains(" bunt")),
            "general_contact_types",
        ] = "bunt"
        df.loc[
            (df.general_contact_types == "triple")
            & (df.at_bat_outcome_description.str.contains(" ground ball")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "triple")
            & (df.at_bat_outcome_description.str.contains(" fly ball")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "triple")
            & (df.at_bat_outcome_description.str.contains(" pop up")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "triple")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "triple")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"
        df.loc[df.general_contact_types == "triple", "general_contact_types"] = "line"

        df.loc[
            (df.general_contact_types == "home run")
            & (df.at_bat_outcome_description.str.contains(" ground ball")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "home run")
            & (df.at_bat_outcome_description.str.contains(" fly ball")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "home run")
            & (df.at_bat_outcome_description.str.contains(" pop up")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "home run")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "home run")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "home run")
            & (df.at_bat_outcome_description.str.contains(" grand slam")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "home run"), "general_contact_types"
        ] = "fly"  # Default remaining homeruns to fly (more likely than line)

        df.loc[
            (df.general_contact_types == "error")
            & (df.at_bat_outcome_description.str.contains(" bunt")),
            "general_contact_types",
        ] = "bunt"
        df.loc[
            (df.general_contact_types == "error")
            & (df.at_bat_outcome_description.str.contains(" ground ball")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "error")
            & (df.at_bat_outcome_description.str.contains(" fly ball")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "error")
            & (df.at_bat_outcome_description.str.contains(" pop up")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "error")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "error")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"
        df.loc[df.general_contact_types == "error", "general_contact_types"] = "ground"

        # Ground Rule Doubles are often not classified appropritely
        df.loc[
            (df.at_bat_outcome_description.str.contains(" ground-rule double"))
            & (df.at_bat_outcome_description.str.contains(" bunt")),
            "general_contact_types",
        ] = "bunt"
        df.loc[
            (df.at_bat_outcome_description.str.contains(" ground-rule double"))
            & (df.at_bat_outcome_description.str.contains(" ground ball")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.at_bat_outcome_description.str.contains(" ground-rule double"))
            & (df.at_bat_outcome_description.str.contains(" fly ball")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.at_bat_outcome_description.str.contains(" ground-rule double"))
            & (df.at_bat_outcome_description.str.contains(" pop up")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.at_bat_outcome_description.str.contains(" ground-rule double"))
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.at_bat_outcome_description.str.contains(" ground-rule double"))
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"

        # Interference often has an actual play associated with it and is not always a non-contact event (interference)
        df.loc[
            (df.general_contact_types == "non_contact")
            & (df.at_bat_outcome_description.str.contains(" bunt")),
            "general_contact_types",
        ] = "bunt"
        df.loc[
            (df.general_contact_types == "non_contact")
            & (df.at_bat_outcome_description.str.contains(" ground ball")),
            "general_contact_types",
        ] = "ground"
        df.loc[
            (df.general_contact_types == "non_contact")
            & (df.at_bat_outcome_description.str.contains(" fly ball")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "non_contact")
            & (df.at_bat_outcome_description.str.contains(" pop up")),
            "general_contact_types",
        ] = "fly"
        df.loc[
            (df.general_contact_types == "non_contact")
            & (df.at_bat_outcome_description.str.contains(" lines")),
            "general_contact_types",
        ] = "line"
        df.loc[
            (df.general_contact_types == "non_contact")
            & (df.at_bat_outcome_description.str.contains(" line drive")),
            "general_contact_types",
        ] = "line"

        # Default Remaining unadjusted hits to "Hit-Unknown" and outs to "Out-Unknown"
        df.loc[
            df.general_contact_types.isin(["single", "double", "triple", "home run"]),
            "general_contact_types",
        ] = "unknown"
        df.loc[
            df.general_contact_types.isin(["forceout", "double play", "triple play"]),
            "general_contact_types",
        ] = "ground"

        # In the way the data is coded, there is no way to know if a fielders choice was a grounder, linedrive, or flyout
        # Assumption: most fielders choices are likely infield gorunders with a forceout to a leading runner
        df.loc[
            (df.general_contact_types == "fielders choice"), "general_contact_types"
        ] = "ground"

        return df

    def get_hit_location(input_string):
        """
        Takes as input an atbat outcome description (string) and ouputs the position player the ball was hit to
        """

        def remove_extra_info_string(input_position_string):
            input_position_string = input_position_string.replace("ground ball", "")
            input_position_string = input_position_string.replace("fly ball", "")
            input_position_string = input_position_string.replace("line drive", "")

            input_position_string = input_position_string.replace(
                " to first base", "first baseman"
            )
            input_position_string = input_position_string.replace(
                " to second base", "second baseman"
            )
            input_position_string = input_position_string.replace(
                " to third base", "third baseman"
            )

            input_position_string = input_position_string.replace("-", " ")
            return input_position_string

        positions = [
            "left field",
            "right field",
            "center field",
            "third baseman",
            "shortstop",
            "second baseman",
            "first baseman",
            "pitcher",
            "catcher",
            "left-field",
            "right-field",
            "center-field",
            "ground ball to first base",
            "ground ball to second base",
            "ground ball to third base",
            "fly ball to first base",
            "fly ball to second base",
            "fly ball to third base",
            "line drive to first base",
            "line drive to second base",
            "line drive to third base",
        ]

        fielders_involved = [input_string.find(position) for position in positions]

        # Find index of the minimum number not equal to -1
        # If all -1 return ("NOT_FIELDED")
        try:
            min_index = fielders_involved.index(
                min(i for i in fielders_involved if i > 0)
            )

            # Handle edge cases
            # Assume sacrifice bunts went to pitcher.
            if "sacrifice bunt" in input_string:
                return "pitcher"

            # Cases where the result is actually "not_fielded" such as intentional walks
            elif any(
                substring in input_string
                for substring in [
                    "intentionally walk",
                    "walks",
                    "strikes out",
                    "called out on strikes",
                ]
            ):
                return "not_fielded"

            else:
                return remove_extra_info_string(positions[min_index])
        except ValueError:
            return "not_fielded"

    col_to_dtype = {
        "date": str,
        "home_team": str,
        "away_team": str,
        "inning_num": int,
        "bat_mlbid": int,
        "pit_mlbid": int,
        "bat_hand": str,
        "pit_hand": str,
        "at_bat_outcome": str,
        "at_bat_outcome_description": str,
        "at_bat_pitch_counter": int,
        "pitch_ax": float,
        "pitch_ay": float,
        "pitch_az": float,
        "pitch_break_angle": float,
        "pitch_break_length": float,
        "pitch_break_y": float,
        "pitch_type": str,
        "pitch_type_confidence": float,
        "pitch_nasty_factor": float,
        "pitch_spin_dir": float,
        "pitch_spin_rate": float,
        "pitch_start_speed": float,
        "pitch_end_speed": float,
        "pitch_pfx_x": float,
        "pitch_pfx_z": float,
        "pitch_px": float,
        "pitch_pz": float,
        "pitch_vx0": float,
        "pitch_vy0": float,
        "pitch_vz0": float,
        "pitch_x": float,
        "pitch_x0": float,
        "pitch_y": float,
        "pitch_y0": float,
        "pitch_z0": float,
        "at_bat_outcome_description": str,
        "pitch_outcome_description": str,
    }

    con = database_mlb_connection()

    pitch_query = read_sql("pymlb/queries/get_pitch_data.sql")
    pitch_query = pitch_query.format(start_date, end_date)

    pitch = pd.read_sql(pitch_query, con)

    # cast dtypes
    for col in pitch.columns:
        pitch[col] = pitch[col].astype(col_to_dtype[col])

    pitch = map_outcomes_to_general_contact_types(pitch)

    # Remove events not tied to atbat such as ejections, catcher interference, pickoffs
    remove_events = ["ejected", "catcher interference", "picked off"]
    pitch = pitch[
        ~(
            pitch.at_bat_outcome_description.str.lower().str.contains(
                "|".join(remove_events)
            )
        )
    ].copy()

    pitch["atbat_key"] = (
        pitch.date.astype(str)
        + pitch.home_team.astype(str)
        + pitch.away_team.astype(str)
        + pitch.inning_num.astype(str)
        + pitch.bat_mlbid.astype(str)
        + pitch.pit_mlbid.astype(str)
    )
    pitch["game_key"] = (
        pitch.date.astype(str)
        + pitch.home_team.astype(str)
        + pitch.away_team.astype(str)
    )

    pitch.pitch_outcome_description.replace("In play, out(s)", "in_play", inplace=True)
    pitch.pitch_outcome_description.replace("Ball In Dirt", "ball", inplace=True)
    pitch.pitch_outcome_description.replace("In play, run(s)", "in_play", inplace=True)
    pitch.pitch_outcome_description.replace("In play, no out", "in_play", inplace=True)
    pitch.pitch_outcome_description.replace("Foul (Runner Going)", "foul", inplace=True)
    pitch.pitch_outcome_description.replace(
        "Swinging Strike (Blocked)", "Swinging Strike", inplace=True
    )

    pitch["atbat_outcome_fielder_location"] = pitch.at_bat_outcome_description.map(
        get_hit_location
    )

    # Map at bat outcomes to general categories
    at_bat_outcomes_mapping = {
        "Bunt Groundout": "out",
        "Bunt Lineout": "out",
        "Bunt Pop Out": "out",
        "Catcher Interference": "interference",
        "Double": "double",
        "Field Error": "error",
        "Fielders Choice Out": "out",
        "Fielders Choice": "out",
        "Flyout": "out",
        "Forceout": "out",
        "Grounded Into DP": "out",
        "Groundout": "out",
        "Hit By Pitch": "walk_hbp",
        "Intent Walk": "walk_intent",
        "Lineout": "out",
        "Pop Out": "out",
        "Sac Bunt": "out",
        "Sac Fly DP": "out",
        "Sac Fly": "out",
        "Strikeout - DP": "strikeout",
        "Triple Play": "out",
        "Triple": "triple",
        "Walk": "walk",
        "Batter Interference": "interference",
        "Double Play": "out",
        "Fan interference": "interference",
        "Home Run": "home_run",
        "Runner Out": "out",
        "Sacrifice Bunt DP": "out",
        "Single": "single",
        "Strikeout": "strikeout",
    }

    pitch["at_bat_outcome_adj"] = pitch.at_bat_outcome.map(at_bat_outcomes_mapping)

    # ground-rule doubles are often miscategorized
    pitch.loc[
        pitch.at_bat_outcome_description.str.contains("ground-rule double"),
        "at_bat_outcome_adj",
    ] = "double"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("homers")),
        "at_bat_outcome_adj",
    ] = "home_run"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("home run")),
        "at_bat_outcome_adj",
    ] = "home_run"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("triples")),
        "at_bat_outcome_adj",
    ] = "triple"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("doubles")),
        "at_bat_outcome_adj",
    ] = "double"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("singles")),
        "at_bat_outcome_adj",
    ] = "single"

    # Interference often resulted in an out
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("pops out")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("grounds out")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("lines out")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("flies out")),
        "at_bat_outcome_adj",
    ] = "out"

    # Default double play to out
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("double play")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_description.str.contains("double play")),
        "at_bat_outcome_adj",
    ] = "out"

    # Default fielding errors to outs
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("fielding error")),
        "at_bat_outcome_adj",
    ] = "out"

    # Strike outs to out
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("strikes out")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("called out on strikes")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("sacrifice bunt")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "interference")
        & (pitch.at_bat_outcome_description.str.contains("throwing error")),
        "at_bat_outcome_adj",
    ] = "out"
    pitch.loc[
        (pitch.at_bat_outcome_description.str.contains("homers"), "at_bat_outcome_adj")
    ] = "home_run"

    # When the at_bat_outcome_adj is a hit but the atbat_outcome_fielder_location is not_fielded, default to NAN
    pitch.loc[
        (pitch.at_bat_outcome_adj == "single")
        & (pitch.atbat_outcome_fielder_location == "not_fielded"),
        "atbat_outcome_fielder_location",
    ] = "NaN"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "double")
        & (pitch.atbat_outcome_fielder_location == "not_fielded"),
        "atbat_outcome_fielder_location",
    ] = "NaN"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "triple")
        & (pitch.atbat_outcome_fielder_location == "not_fielded"),
        "atbat_outcome_fielder_location",
    ] = "NaN"
    pitch.loc[
        (pitch.at_bat_outcome_adj == "home_run")
        & (pitch.atbat_outcome_fielder_location == "not_fielded"),
        "atbat_outcome_fielder_location",
    ] = "NaN"

    # https://fastballs.wordpress.com/2007/08/02/glossary-of-the-gameday-pitch-fields/
    response = [
        "atbat_key",
        "date",
        "home_team",
        "away_team",
        "inning_num",
        "at_bat_pitch_counter",
        "at_bat_outcome_description",
        "at_bat_outcome_adj",
        "atbat_outcome_fielder_location",
        "general_contact_types",
    ]
    features = [
        "bat_mlbid",
        "pit_mlbid",
        "bat_hand",
        "pit_hand",
        "pitch_ax",  # acceleration, feet per second, measured at the initial point
        "pitch_ay",  # acceleration, feet per second, measured at the initial point
        "pitch_az",  # acceleration, feet per second, measured at the initial point
        "pitch_break_angle",
        "pitch_break_length",
        "pitch_break_y",
        "pitch_type",
        "pitch_type_confidence",
        "pitch_nasty_factor",
        "pitch_spin_dir",
        "pitch_spin_rate",
        "pitch_start_speed",
        "pitch_end_speed",
        "pitch_pfx_x",  # horizontal movement, inches, of the pitch between the release point and home plate, as compared to a theoretical pitch thrown at the same speed with no spin-induced movement. This parameter is measured at y=40 feet regardless of the y0 value.
        "pitch_pfx_z",  # verticle movement, inches, of the pitch between the release point and home plate, as compared to a theoretical pitch thrown at the same speed with no spin-induced movement. This parameter is measured at y=40 feet regardless of the y0 value
        "pitch_px",  # the left/right distance, in feet, of the pitch from the middle of the plate as it crossed home plate. The PITCHf/x coordinate system is oriented to the catcher’s/umpire’s perspective, with distances to the right being positive and to the left being negative.
        "pitch_pz",  # the height of the pitch in feet as it crossed the front of home plate.
        "pitch_vx0",  # Velocity, feet per sec, measured at the initial point
        "pitch_vy0",  # Velocity, feet per sec, measured at the initial point
        "pitch_vz0",  # Velocity, feet per sec, measured at the initial point
        "pitch_x",  # the horizontal location of the pitch as it crossed home plate
        "pitch_x0",  # the left/right distance, in feet, of the pitch, measured at the initial point.
        "pitch_y",  # the vertical location of the pitch as it crossed home plate
        "pitch_y0",  # the distance in feet from home plate where the PITCHf/x system is set to measure the initial parameters.
        "pitch_z0",
    ]  # the height, in feet, of the pitch, measured at the initial point

    # limit to only relevant columns
    pitch = pitch[response + features]

    # Add one hots for relevant columns
    # Handedness
    pitch["bat_hand"] = pitch.bat_hand.replace("R", "bat_R")
    pitch["bat_hand"] = pitch.bat_hand.replace("L", "bat_L")
    pitch["pit_hand"] = pitch.pit_hand.replace("R", "pit_R")
    pitch["pit_hand"] = pitch.pit_hand.replace("L", "pit_L")

    pitch["year"] = pd.to_datetime(pitch.date).dt.year
    pitch["date"] = pd.to_datetime(pitch.date).dt.date

    return pitch
