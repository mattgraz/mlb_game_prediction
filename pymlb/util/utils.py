import json
import pandas as pd
import numpy as np


def get_update_dates():
    with open("update_dates_dict.txt", "r") as f:
        update_dates_dict = json.load(f)
    return update_dates_dict


def get_config():
    with open("config.json", "r") as f:
        config = json.load(f)
    return config


# https://github.com/pandas-dev/pandas/issues/11250
def get_duplicate_columns(frame):
    groups = frame.columns.to_series().groupby(frame.dtypes).groups
    dups = []

    for t, v in groups.items():

        cs = frame[v].columns
        vs = frame[v]
        lcs = len(cs)

        for i in range(lcs):
            ia = vs.iloc[:, i].values
            for j in range(i + 1, lcs):
                ja = vs.iloc[:, j].values
                if np.array_equal(ia, ja):
                    dups.append(cs[i])
                    break

    return dups


# Define a dictionary mapping of event types to more general categories
old_event_to_new_dict = {
    "Batter Interference": "Interference",
    "Bunt Ground Out": "Out-Other",
    "Bunt Groundout": "Out-Other",
    "Bunt Lineout": "Out-Other",
    "Bunt Pop Out": "Out-Other",
    "Catcher Interference": "Interference",
    "Double": "Double",
    "Double Play": "Double Play",
    "Fan interference": "Interference",
    "Field Error": "Error",
    "Fielders Choice": "Fielders Choice",
    "Fielders Choice Out": "Fielders Choice",
    "Fly Out": "Out-Fly",
    "Flyout": "Out-Fly",
    "Force Out": "Forceout",
    "Forceout": "Forceout",
    "Ground Out": "Out-Ground",
    "Grounded Into DP": "Out-Ground",
    "Groundout": "Out-Ground",
    "Hit By Pitch": "Walk",
    "Home Run": "Home Run",
    "Intent Walk": "Intentional Walk",
    "Line Out": "Out-Line",
    "Lineout": "Out-Line",
    "Pop Out": "Out-Fly",
    "Runner Out": "Out-Other",
    "Sac Bunt": "Sacrifice",
    "Sac Fly": "Out-Fly",
    "Sac Fly DP": "Out-Fly",
    "Sacrifice Bunt DP": "Sacrifice",
    "Single": "Single",
    "Strikeout": "Strikeout",
    "Strikeout - DP": "Strikeout",
    "Triple": "Triple",
    "Triple Play": "Triple Play",
    "Walk": "Walk",
}
