from bs4 import BeautifulSoup
from urllib.request import urlopen
import pandas as pd
from copy import deepcopy
from time import sleep
from numpy import NaN


def scrape_batter_daily_aggregate(file_url, input_date):
    """
    PURPOSE:

    Inputs:
        file_url: String of a url to download
        date: Datetime object

    Output:
        dictionary containing mapping of row name to value

    """
    # Load batter file
    batter_info_soup = BeautifulSoup(urlopen(file_url), "lxml")

    # Pull batter attributes,
    batter_attrs = batter_info_soup.body.batting.attrs

    new_row = {}
    # Get batter_id
    new_row["date"] = input_date
    new_row["bat_mlbid"] = file_url[0][-12:-6]
    new_row["ab"] = batter_attrs.get("ab", "")
    new_row["r"] = batter_attrs.get("r", "")
    new_row["h"] = batter_attrs.get("h", "")
    new_row["hr"] = batter_attrs.get("hr", "")
    new_row["rbi"] = batter_attrs.get("rbi", "")
    new_row["sb"] = batter_attrs.get("sb", "")
    new_row["avg"] = batter_attrs.get("avg", "")
    new_row["slg"] = batter_attrs.get("slg", "")
    new_row["obp"] = batter_attrs.get("obp", "")
    new_row["ops"] = batter_attrs.get("ops", "")
    new_row["pa"] = batter_attrs.get("pa", "")
    new_row["sf"] = batter_attrs.get("sf", "")
    new_row["sac"] = batter_attrs.get("sac", "")
    new_row["ci"] = batter_attrs.get("ci", "")
    new_row["ibb"] = batter_attrs.get("ibb", "")
    new_row["single"] = batter_attrs.get("single", "")
    new_row["double"] = batter_attrs.get("double", "")
    new_row["triple"] = batter_attrs.get("triple", "")
    new_row["bb"] = batter_attrs.get("bb", "")
    new_row["cs"] = batter_attrs.get("cs", "")
    new_row["so"] = batter_attrs.get("so", "")
    new_row["err"] = batter_attrs.get("err", "")
    new_row["hbp"] = batter_attrs.get("hbp", "")
    new_row["s_ab"] = batter_attrs.get("s_ab", "")
    new_row["s_pa"] = batter_attrs.get("s_pa", "")
    new_row["s_sf"] = batter_attrs.get("s_sf", "")
    new_row["s_sac"] = batter_attrs.get("s_sac", "")
    new_row["s_ci"] = batter_attrs.get("s_ci", "")
    new_row["s_ibb"] = batter_attrs.get("s_ibb", "")
    new_row["s_hr"] = batter_attrs.get("s_hr", "")
    new_row["s_rbi"] = batter_attrs.get("s_rbi", "")
    new_row["s_sb"] = batter_attrs.get("s_sb", "")
    new_row["s_h"] = batter_attrs.get("s_h", "")
    new_row["s_r"] = batter_attrs.get("s_r", "")
    new_row["s_single"] = batter_attrs.get("s_single", "")
    new_row["s_double"] = batter_attrs.get("s_double", "")
    new_row["s_triple"] = batter_attrs.get("s_triple", "")
    new_row["s_bb"] = batter_attrs.get("s_bb", "")
    new_row["s_cs"] = batter_attrs.get("s_cs", "")
    new_row["s_so"] = batter_attrs.get("s_so", "")
    new_row["s_err"] = batter_attrs.get("s_err", "")
    new_row["s_hbp"] = batter_attrs.get("s_hbp", "")
    new_row["game_id"] = batter_attrs.get("game_id", "")
    new_row["game_pk"] = batter_attrs.get("game_pk", "")
    new_row["update_AB"] = batter_attrs.get("update_AB", "")

    return new_row


def scrape_batter_game(file_url, input_date):
    """
    PURPOSE:

    Inputs:
        file_url: String of a url to download
        date: Datetime object

    Output:
        dictionary containing mapping of row name to value

    """
    counter = 0
    connection = False
    while connection == False & counter < 10:
        try:
            # Scrape data
            batter_info_soup = BeautifulSoup(urlopen(file_url), "lxml")
            connection = True
        except:
            print("Connection failed! \n\n Retrying ({})...".format(counter))
            sleep(2)
    # Pull batter attributes,
    try:
        batter_personal_stats = deepcopy(batter_info_soup.body.player.attrs)
    except:
        batter_personal_stats = {}

    try:
        batter_season_stats = deepcopy(batter_info_soup.body.player.season.attrs)
    except:
        batter_season_stats = {}

    try:
        batter_career_stats = deepcopy(batter_info_soup.body.player.career.attrs)
    except:
        batter_career_stats = {}

    try:
        batter_month_stats = deepcopy(batter_info_soup.body.player.month.attrs)
    except:
        batter_month_stats = {}

    try:
        batter_team_stats = deepcopy(batter_info_soup.body.player.team.attrs)
    except:
        batter_team_stats = {}

    try:
        batter_bases_empty_stats = deepcopy(batter_info_soup.body.player.empty.attrs)
    except:
        batter_bases_empty_stats = {}

    try:
        batter_men_on_stats = deepcopy(batter_info_soup.body.player.men_on.attrs)
    except:
        batter_men_on_stats = {}

    try:
        batter_risp_stats = deepcopy(batter_info_soup.body.player.risp.attrs)
    except:
        batter_risp_stats = {}

    try:
        batter_loaded_stats = deepcopy(batter_info_soup.body.player.loaded.attrs)
    except:
        batter_loaded_stats = {}

    try:
        batter_vs_lhp_stats = deepcopy(batter_info_soup.body.player.vs_lhp.attrs)
    except:
        batter_vs_lhp_stats = {}

    try:
        batter_vs_rhp_stats = deepcopy(batter_info_soup.body.player.vs_rhp.attrs)
    except:
        batter_vs_rhp_stats = {}

    try:
        batter_vs_p_stats = deepcopy(batter_info_soup.body.player.vs_p.attrs)
    except:
        batter_vs_p_stats = {}
    # batter_vs_p5_stats = deepcopy(batter_info_soup.body.player.vs_p5.attrs)

    try:
        batter_pitch_stats = deepcopy(batter_info_soup.body.player.pitch.attrs)
    except:
        batter_pitch_stats = {}

    key_append_strings = [
        "season",
        "career",
        "month",
        "team",
        "bases_empty",
        "men_on",
        "risp",
        "loaded",
        "vs_lhp",
        "vs_rhp",
        "vs_p",
        "pitch",
    ]

    dicts = [
        batter_season_stats,
        batter_career_stats,
        batter_month_stats,
        batter_team_stats,
        batter_bases_empty_stats,
        batter_men_on_stats,
        batter_risp_stats,
        batter_loaded_stats,
        batter_vs_lhp_stats,
        batter_vs_rhp_stats,
        batter_vs_p_stats,
        batter_pitch_stats,
    ]

    for key_addition, iter_dict in zip(key_append_strings, dicts):
        for key in list(iter_dict.keys()):
            iter_dict[key_addition + "_" + key] = iter_dict.pop(key)

    # Join all dictionaries
    all_data = {}
    for iter_dict in dicts:
        all_data.update(iter_dict)

    all_data["date"] = input_date

    try:
        all_data["bat_mlbid"] = batter_personal_stats["id"]
    except:
        print("NO PLAYER ID FOUND")
        all_data = {}

    return all_data


def scrape_pitcher_game(file_url, input_date):
    """
    PURPOSE:

    Inputs:
        file_url: String of a url to download
        date: Datetime object

    Output:
        dictionary containing mapping of row name to value

    """
    counter = 0
    connection = False
    while connection == False & counter < 10:
        try:
            # Scrape data
            pitcher_info_soup = BeautifulSoup(urlopen(file_url), "lxml")
            connection = True
        except:
            print("Connection failed! \n\n Retrying ({})...".format(counter))
            sleep(2)
    # Pull pitcher attributes,
    try:
        pitcher_personal_stats = deepcopy(pitcher_info_soup.body.player.attrs)
    except:
        pitcher_personal_stats = {}

    try:
        pitcher_season_stats = deepcopy(pitcher_info_soup.body.player.season.attrs)
    except:
        pitcher_season_stats = {}

    try:
        pitcher_career_stats = deepcopy(pitcher_info_soup.body.player.career.attrs)
    except:
        pitcher_career_stats = {}

    try:
        pitcher_month_stats = deepcopy(pitcher_info_soup.body.player.month.attrs)
    except:
        pitcher_month_stats = {}

    try:
        pitcher_team_stats = deepcopy(pitcher_info_soup.body.player.team.attrs)
    except:
        pitcher_team_stats = {}

    try:
        pitcher_bases_empty_stats = deepcopy(pitcher_info_soup.body.player.empty.attrs)
    except:
        pitcher_bases_empty_stats = {}

    try:
        pitcher_men_on_stats = deepcopy(pitcher_info_soup.body.player.men_on.attrs)
    except:
        pitcher_men_on_stats = {}

    try:
        pitcher_risp_stats = deepcopy(pitcher_info_soup.body.player.risp.attrs)
    except:
        pitcher_risp_stats = {}

    try:
        pitcher_loaded_stats = deepcopy(pitcher_info_soup.body.player.loaded.attrs)
    except:
        pitcher_loaded_stats = {}

    try:
        pitcher_vs_lhb_stats = deepcopy(pitcher_info_soup.body.player.vs_lhb.attrs)
    except:
        pitcher_vs_lhb_stats = {}

    try:
        pitcher_vs_rhb_stats = deepcopy(pitcher_info_soup.body.player.vs_rhb.attrs)
    except:
        pitcher_vs_rhb_stats = {}

    try:
        pitcher_vs_b_stats = deepcopy(pitcher_info_soup.body.player.vs_b.attrs)
    except:
        pitcher_vs_b_stats = {}
    # pitcher_vs_b5_stats = deepcopy(pitcher_info_soup.body.player.vs_b5.attrs)

    try:
        pitcher_pitch_stats = deepcopy(pitcher_info_soup.body.player.pitch.attrs)
    except:
        pitcher_pitch_stats = {}

    key_append_strings = [
        "season",
        "career",
        "month",
        "team",
        "bases_empty",
        "men_on",
        "risp",
        "loaded",
        "vs_lhb",
        "vs_rhb",
        "vs_b",
        "pitch",
    ]

    dicts = [
        pitcher_season_stats,
        pitcher_career_stats,
        pitcher_month_stats,
        pitcher_team_stats,
        pitcher_bases_empty_stats,
        pitcher_men_on_stats,
        pitcher_risp_stats,
        pitcher_loaded_stats,
        pitcher_vs_lhb_stats,
        pitcher_vs_rhb_stats,
        pitcher_vs_b_stats,
        pitcher_pitch_stats,
    ]

    for key_addition, iter_dict in zip(key_append_strings, dicts):
        for key in list(iter_dict.keys()):
            iter_dict[key_addition + "_" + key] = iter_dict.pop(key)

    # Join all dictionaries
    all_data = {}
    for iter_dict in dicts:
        all_data.update(iter_dict)

    all_data["date"] = input_date

    try:
        all_data["pit_mlbid"] = pitcher_personal_stats["id"]
    except:
        print("NO PLAYER ID FOUND")
        all_data = {}

    return all_data


def float_robust(num):
    """
    Purpose:
        attempts to convert input into float_robust and returns np.NaN if it cannot
    """
    try:
        out = float(num)
    except:
        out = NaN
    return out


def scrape_pitchfx_game(file_url, input_date):
    """
    PURPOSE:

    Inputs:
        file_url: String of a url to download
        date: Datetime object

    Output:
        dataframe containing mapping of all pitches in an input url

    """

    cols = [
        "date",
        "home_team",
        "away_team",
        "inning_num",
        "bat_mlbid",
        "pit_mlbid",
        "bat_hand",
        "pit_hand",
        "away_team_runs",
        "home_team_runs",
        "at_bat_outcome",
        "at_bat_outcome_description",
        "at_bat_pitch_counter",
        "pitch_ax",
        "pitch_ay",
        "pitch_az",
        "pitch_break_angle",
        "pitch_break_length",
        "pitch_break_y",
        "pitch_batting_count_type",
        "pitch_type",
        "pitch_type_confidence",
        "pitch_outcome_description",
        "pitch_nasty_factor",
        "pitch_pfx_x",
        "pitch_pfx_z",
        "pitch_px",
        "pitch_pz",
        "pitch_spin_dir",
        "pitch_spin_rate",
        "pitch_start_speed",
        "pitch_end_speed",
        "pitch_vx0",
        "pitch_vy0",
        "pitch_vz0",
        "pitch_x",
        "pitch_x0",
        "pitch_y",
        "pitch_y0",
        "pitch_z0",
        "pitch_zone",
        "pitch_time_zulu",
    ]

    game_soup = BeautifulSoup(urlopen_robust(file_url), "lxml")
    new_rows = pd.DataFrame(columns=cols)
    # Iterate through each game inning
    for inning in game_soup.find_all("game")[0].find_all("inning"):
        inning_attrs = inning.attrs

        home_team = inning_attrs["home_team"]
        away_team = inning_attrs["away_team"]
        inning_number = int(inning_attrs["num"])

        # Define inning halves (catches if the bottom of 9th is not played)
        inning_halves = [inning.find_all("top"), inning.find_all("bottom")]
        inning_halves = [item for sublist in inning_halves for item in sublist]

        # Iterate thorugh the top and bottom of the inning
        for inning_half in inning_halves:

            # Iterate through each atbat
            for at_bat in inning_half.find_all("atbat"):
                at_bat_attrs = at_bat.attrs

                bat_mlbid = at_bat_attrs["batter"]
                bat_hand = at_bat_attrs.get("stand", "")
                pit_mlbid = at_bat_attrs["pitcher"]
                pit_hand = at_bat_attrs.get("p_throws", "")
                away_team_runs = int(
                    at_bat_attrs.get("away_team_runs", -1)
                )  # runs pre at-bat
                home_team_runs = int(
                    at_bat_attrs.get("home_team_runs", -1)
                )  # runs pre at-bat
                at_bat_outcome = at_bat_attrs.get("event", "")
                at_bat_outcome_description = at_bat_attrs.get("des", "").strip()

                # Iterate through each pitch
                at_bat_pitch_counter = 0
                for pitch in at_bat.find_all("pitch"):
                    pitch_attrs = pitch.attrs

                    at_bat_pitch_counter = at_bat_pitch_counter + 1
                    pitch_ax = float_robust(pitch_attrs.get("ax", NaN))
                    pitch_ay = float_robust(pitch_attrs.get("ay", NaN))
                    pitch_az = float_robust(pitch_attrs.get("az", NaN))
                    pitch_break_angle = float_robust(
                        pitch_attrs.get("break_angle", NaN)
                    )
                    pitch_break_length = float_robust(
                        pitch_attrs.get("break_length", NaN)
                    )
                    pitch_break_y = float_robust(pitch_attrs.get("break_y", NaN))
                    pitch_batting_count_type = pitch_attrs.get("type", "")
                    pitch_type = pitch_attrs.get("pitch_type", "")
                    pitch_type_confidence = float_robust(
                        pitch_attrs.get("type_confidence", NaN)
                    )
                    pitch_outcome_description = pitch_attrs.get("des", "")
                    pitch_nasty_factor = float_robust(pitch_attrs.get("nasty", NaN))
                    pitch_pfx_x = float_robust(pitch_attrs.get("pfx_x", NaN))
                    pitch_pfx_z = float_robust(pitch_attrs.get("pfx_z", NaN))
                    pitch_px = float_robust(pitch_attrs.get("px", NaN))
                    pitch_pz = float_robust(pitch_attrs.get("pz", NaN))
                    pitch_spin_dir = float_robust(pitch_attrs.get("spin_dir", NaN))
                    pitch_spin_rate = float_robust(pitch_attrs.get("spin_rate", NaN))
                    pitch_start_speed = float_robust(
                        pitch_attrs.get("start_speed", NaN)
                    )
                    pitch_end_speed = float_robust(pitch_attrs.get("end_speed", NaN))
                    pitch_vx0 = float_robust(pitch_attrs.get("vx0", NaN))
                    pitch_vy0 = float_robust(pitch_attrs.get("vy0", NaN))
                    pitch_vz0 = float_robust(pitch_attrs.get("vz0", NaN))
                    try:
                        pitch_x = float_robust(pitch_attrs.get("x", NaN))
                    except:
                        print("OHHH NOOOOO")
                        print(pitch_attrs.get("x", NaN))
                        print(pitch_attrs)
                        # print(inning, inning_half, at_bat, pitch)
                        break
                    pitch_x0 = float_robust(pitch_attrs.get("x0", NaN))
                    pitch_y = float_robust(pitch_attrs.get("y", NaN))
                    pitch_y0 = float_robust(pitch_attrs.get("y0", NaN))
                    pitch_z0 = float_robust(pitch_attrs.get("z0", NaN))
                    pitch_zone = float_robust(pitch_attrs.get("zone", NaN))
                    pitch_time_zulu = pitch_attrs.get("start_tfs_zulu", "")

                    # Append pitch to dataframe
                    new_row = pd.DataFrame(
                        [
                            [
                                input_date,
                                home_team,
                                away_team,
                                inning_number,
                                bat_mlbid,
                                pit_mlbid,
                                bat_hand,
                                pit_hand,
                                away_team_runs,
                                home_team_runs,
                                at_bat_outcome,
                                at_bat_outcome_description,
                                at_bat_pitch_counter,
                                pitch_ax,
                                pitch_ay,
                                pitch_az,
                                pitch_break_angle,
                                pitch_break_length,
                                pitch_break_y,
                                pitch_batting_count_type,
                                pitch_type,
                                pitch_type_confidence,
                                pitch_outcome_description,
                                pitch_nasty_factor,
                                pitch_pfx_x,
                                pitch_pfx_z,
                                pitch_px,
                                pitch_pz,
                                pitch_spin_dir,
                                pitch_spin_rate,
                                pitch_start_speed,
                                pitch_end_speed,
                                pitch_vx0,
                                pitch_vy0,
                                pitch_vz0,
                                pitch_x,
                                pitch_x0,
                                pitch_y,
                                pitch_y0,
                                pitch_z0,
                                pitch_zone,
                                pitch_time_zulu,
                            ]
                        ],
                        columns=cols,
                    )

                    new_rows = new_rows.append(new_row)

    return new_rows


def urlopen_robust(input_url, max_connection_attempts=5):
    """
    PURPOSE: This is a wrapper for urlopen that attempts up to N reconnects if the connection fails
    """

    # Collect games on the date
    success = False
    counter = 0
    connection = ""
    while (not success) & (counter < max_connection_attempts):
        try:
            connection = urlopen(input_url)
            success = True
        except:
            print("Connection failed! \n\n Retrying ({})...".format(counter))
            counter = counter + 1
            sleep(2)

    return connection
