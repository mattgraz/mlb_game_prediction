import pandas as pd
import numpy as np
from datetime import datetime, date, timedelta
from copy import deepcopy

from pymlb.util.utils import old_event_to_new_dict

# Import Database Connection
from pymlb.util.database import (
    database_mlb_engine,
    database_mlb_cursor,
    database_mlb_connection,
    read_sql,
)

engine = database_mlb_engine()
cur = database_mlb_cursor()
con = database_mlb_connection()


def pitcher_pitch_rate_counts(mlb_id, bat_hand, start_date, end_date):
    """
    PURPOSE: For a given data_through_date, career pitch counts and pitch rates are
             calculated for a given pitcher by L/R batter

    INPUT:
        mlb_id:
            The mlb id a player or list of players (for team statistics)
            This is a list/tuple of integers

        bat_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string)
    OUTPUT:
        Pandas dataframe containing pitcher pitch counts and rates

    """

    # Generage query strings to input into sql statement
    mlbid_query_string = ", ".join("'" + str(item) + "'" for item in mlb_id)
    bat_hand_query_string = ", ".join("'" + item + "'" for item in bat_hand)

    query = read_sql("pymlb/queries/pitcher_pitchfx_pitch_counts.sql")
    query = query.format(
        mlbid_query_string, bat_hand_query_string, start_date, end_date
    )
    player_pitching = pd.read_sql(query, con)

    pitcher_data_total_pitches = (
        player_pitching.groupby(["pit_mlbid"]).sum().reset_index()
    )
    pitcher_data_total_pitches.columns = ["pit_mlbid", "total_pitch_count"]

    player_pitching = player_pitching.merge(pitcher_data_total_pitches)
    player_pitching["pitch_rate"] = (
        player_pitching["pitch_count"] / player_pitching["total_pitch_count"]
    )

    # If a pitch has been thrown less than 1% of the time, remove those pitches from the dataset
    remove_from_total = player_pitching[
        player_pitching.pitch_rate <= .01
    ].pitch_count.sum()
    player_pitching = player_pitching[player_pitching.pitch_rate > .01]

    player_pitching["total_pitch_count"] = (
        player_pitching.total_pitch_count - remove_from_total
    )
    player_pitching["pitch_rate"] = (
        player_pitching["pitch_count"] / player_pitching["total_pitch_count"]
    )

    # Rename all pitch_types to pitch_type_<pitch>_rate before pivot
    player_pitching["pitch_type"] = "pitch_type_" + player_pitching.pitch_type

    # Pivot data from long to wide
    player_pitching_pivot = (
        player_pitching[["pit_mlbid", "pitch_type", "pitch_rate"]]
        .pivot("pit_mlbid", "pitch_type")
        .reset_index()
    )

    player_pitching_pivot = player_pitching_pivot.pitch_rate.reset_index(drop=True)

    return player_pitching_pivot


def pitching_batting_against_statistics(mlb_id, bat_hand, start_date, end_date):
    """
    Purpose:
        Calculates relevant batter statistics against a pitcher for an input timeframe and per a set of input filters

    Inputs:

        mlb_id:
            The mlb id a player or list of players (for team statistics)
            This is a list/tuple of integers

        bat_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string)

    """

    # Generage query strings to input into sql statement
    mlbid_query_string = ", ".join("'" + str(item) + "'" for item in mlb_id)
    bat_hand_query_string = ", ".join("'" + item + "'" for item in bat_hand)

    query = read_sql("pymlb/queries/pitching_batting_against_statistics.sql")
    query = query.format(
        mlbid_query_string, bat_hand_query_string, start_date, end_date
    )
    player_batting = pd.read_sql(query, con)

    if player_batting.shape[0] == 0:
        print("No rows found")
        return pd.DataFrame(
            columns=[
                "mlb_id",
                "bat_hand",
                "hits_against_count",
                "bb_against_count",
                "bat_avg_against",
                "babip_against",
                "obp_against",
                "slg_against",
                "iso_against",
                "k_rate_against",
                "bb_rate_against",
                "bb_k_rate_against",
                "contact_rate_against",
            ]
        )

    # Get event counts of player
    player_batting = player_batting.assign(event_counts=1)
    player_event_counts = (
        player_batting[["at_bat_outcome", "event_counts"]]
        .groupby(["at_bat_outcome"])
        .count()
        .reset_index()
    )

    # General helper calculations used downstream
    at_bats_events_exclude_list = [
        "Walk",
        "Intent Walk",
        "Hit By Pitch",
        "Sac Fly",
        "Sac Fly DP",
        "Sacrifice Fly",
        "Sacrifice Fly DP",
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
    ]

    plate_appearances_events_exclude_list = [
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
    ]

    player_at_bat_counts = np.sum(
        player_event_counts[
            ~(player_event_counts.at_bat_outcome.isin(at_bats_events_exclude_list))
        ].event_counts
    )
    player_plate_appearance_counts = np.sum(
        player_event_counts[
            ~(
                player_event_counts.at_bat_outcome.isin(
                    plate_appearances_events_exclude_list
                )
            )
        ].event_counts
    )

    # BATTING AVERAGE
    hits_events_list = ["Single", "Double", "Triple", "Home Run"]
    try:
        hits_against_count = np.sum(
            player_event_counts[
                (player_event_counts.at_bat_outcome.isin(hits_events_list))
            ].event_counts
        )
        player_batting_average = hits_against_count / player_at_bat_counts
    except ZeroDivisionError:
        hits_against_count = 0
        player_batting_average = np.nan

    # BATTING AVERAGE BALLS IN PLAY
    balls_in_play_denominator_exclude_list = [
        "Walk",
        "Intent Walk",
        "Hit By Pitch",
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
        "Strikeout",
        "Home Run",
    ]
    try:
        player_batting_average_balls_in_play = np.sum(
            player_event_counts[
                (
                    player_event_counts.at_bat_outcome.isin(
                        ["Single", "Double", "Triple"]
                    )
                )
            ].event_counts
        ) / np.sum(
            player_event_counts[
                ~(
                    player_event_counts.at_bat_outcome.isin(
                        balls_in_play_denominator_exclude_list
                    )
                )
            ].event_counts
        )
    except ZeroDivisionError:
        player_batting_average_balls_in_play = np.nan

    # ON BASE PERCENTAGE
    # On Base Percentage: (Hits + Walks + Hit by Pitch) / (At Bats + Walks + Hit by Pitch + Sacrifice Flies
    obp_events_list = [
        "Single",
        "Double",
        "Triple",
        "Home Run",
        "Walk",
        "Hit By Pitch",
        "Intent Walk",
    ]
    try:
        player_on_base_percentage = (
            np.sum(
                player_event_counts[
                    (player_event_counts.at_bat_outcome.isin(obp_events_list))
                ].event_counts
            )
            / player_plate_appearance_counts
        )
    except ZeroDivisionError:
        player_on_base_percentage = np.nan

    # SLUGGING PERCENTAGE #
    # Slugging Percentage: ([Singles] + [Doubles x 2] + [Triples x 3] + [Home Runs x 4]) / [At Bats]
    player_singles_count = np.sum(
        player_event_counts[player_event_counts.at_bat_outcome == "Single"].event_counts
    )
    player_doubles_count = np.sum(
        player_event_counts[player_event_counts.at_bat_outcome == "Double"].event_counts
    )
    player_triples_count = np.sum(
        player_event_counts[player_event_counts.at_bat_outcome == "Triple"].event_counts
    )
    player_homerun_count = np.sum(
        player_event_counts[
            player_event_counts.at_bat_outcome == "Home Run"
        ].event_counts
    )

    try:
        player_slugging_percentage = (
            np.sum(
                player_singles_count
                + player_doubles_count * 2
                + player_triples_count * 3
                + player_homerun_count * 4
            )
            / player_at_bat_counts
        )
    except ZeroDivisionError:
        player_slugging_percentage = np.nan

    # ISOLATED POWER #
    # ISO: ([Doubles] + [Triples x 2] + [Home Runs x 3]) / [At Bats] = SLG - AVG
    try:
        player_isolated_power = (
            np.sum(
                player_doubles_count
                + player_triples_count * 2
                + player_homerun_count * 3
            )
            / player_at_bat_counts
        )
    except ZeroDivisionError:
        player_isolated_power = np.nan

    # Strikout Rate
    try:
        player_strikout_rate = (
            np.sum(
                player_event_counts[
                    player_event_counts.at_bat_outcome.isin(
                        ["Strikeout", "Strikeout - DP"]
                    )
                ].event_counts
            )
            / player_plate_appearance_counts
        )
    except ZeroDivisionError:
        player_strikout_rate = np.nan

    # Walk Rate
    try:
        bb_against_count = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Walk"])
            ].event_counts
        )
        player_walk_rate = bb_against_count / player_plate_appearance_counts
    except ZeroDivisionError:
        bb_against_count = 0
        player_walk_rate = np.nan

    # Walk to Strikout Ratio
    try:
        player_walk_to_strikeout_ratio = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Walk"])
            ].event_counts
        ) / np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Strikeout", "Strikeout - DP"])
            ].event_counts
        )
    except ZeroDivisionError:
        player_walk_to_strikeout_ratio = np.nan

    # Contact Rate
    contact_events_list = [
        "Single",
        "Double",
        "Triple",
        "Home Run",
        "Groundout",
        "Lineout",
        "Flyout",
    ]
    contact_events_denominator_exclude_list = [
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
        "Walk",
        "Intent Walk",
        "Hit By Pitch",
    ]

    try:
        player_contact_rate = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(contact_events_list)
            ].event_counts
        ) / np.sum(
            player_event_counts[
                ~(
                    player_event_counts.at_bat_outcome.isin(
                        contact_events_denominator_exclude_list
                    )
                )
            ].event_counts
        )
    except ZeroDivisionError:
        player_contact_rate = np

    # HomeRun Rate
    try:
        player_home_run_rate = (
            np.sum(
                player_event_counts[
                    player_event_counts.at_bat_outcome.isin(["Home Run"])
                ].event_counts
            )
            / player_plate_appearance_counts
        )
    except ZeroDivisionError:
        player_home_run_rate = np.nan

    # HomeRun per flyball Rate
    try:
        player_home_run_per_flyball_rate = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Home Run"])
            ].event_counts
        ) / np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Home Run", "Fly ball"])
            ].event_counts
        )
    except ZeroDivisionError:
        player_home_run_per_flyball_rate = np.nan

    # Add all calculations to data frame
    df = pd.DataFrame(
        data=[
            [
                str(mlb_id),
                str(bat_hand),
                hits_against_count,
                bb_against_count,
                player_batting_average,
                player_batting_average_balls_in_play,
                player_on_base_percentage,
                player_slugging_percentage,
                player_isolated_power,
                player_strikout_rate,
                player_walk_rate,
                player_walk_to_strikeout_ratio,
                player_contact_rate,
            ]
        ],
        columns=[
            "mlb_id",
            "bat_hand",
            "pit_hits_against_count",
            "pit_bb_against_count",
            "pit_bat_avg_against",
            "pit_babip_against",
            "pit_obp_against",
            "pit_slg_against",
            "pit_iso_against",
            "pit_k_rate_against",
            "pit_bb_rate_against",
            "pit_bb_k_rate_against",
            "pit_contact_rate_against",
        ],
    )
    return df


def pitching_statistics(mlb_id, bat_hand, start_date, end_date):
    """
    Purpose:
        Calculates relevant pitching statistics for an input timeframe and per a set of input filters

    Inputs:

        mlb_id:
            The mlb id a player or list of players (for team statistics)
            This is a list/tuple of integers

        bat_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string)

    """

    # Generage query strings to input into sql statement
    mlbid_query_string = ", ".join("'" + str(item) + "'" for item in mlb_id)
    bat_hand_query_string = ", ".join("'" + item + "'" for item in bat_hand)

    query = read_sql("pymlb/queries/pitching_statistics.sql")
    query = query.format(
        mlbid_query_string, bat_hand_query_string, start_date, end_date
    )
    player_pitching = pd.read_sql(query, con)

    if player_pitching.shape[0] == 0:
        print("No rows found")
        return pd.DataFrame(
            columns=[
                "mlb_id",
                "bat_hand",
                "innings_pitched",
                "avg_innings_per_start",
                "avg_innings_per_relief",
                "fip",
                "whip",
            ]
        )

    player_pitching = player_pitching.assign(event_counts=1)
    player_event_counts = (
        player_pitching[["at_bat_outcome", "event_counts"]]
        .groupby(["at_bat_outcome"])
        .count()
        .reset_index()
    )

    # PITCHING METRICS
    # Define if out recorded on a given event to help calculate Innings pitched
    single_out_events = [
        "Bunt Groundout",
        "Bunt Lineout",
        "Fielders Choice Out",
        "Forceout",
        "Groundout",
        "Lineout",
        "Pop Out",
        "Sac Bunt",
        "Sacrifice Bunt DP",
        "Strikeout",
        "Batter Interference",
        "Bunt Groundout",
        "Bunt Pop Out",
        "Flyout",
        "Forceout",
        "Runner Out",
        "Sac Fly",
        "Runner Out",
        "Fielders Choice",
    ]
    double_out_events = ["Double Play", "Grounded Into DP", "Strikeout - DP"]
    triple_out_events = ["Triple Play"]

    # Assign a value of 0, 1, 2, or 3 for hte number of ours recorded for a given even
    player_pitching["single_out"] = np.where(
        player_pitching.at_bat_outcome.isin(single_out_events), 1, np.nan
    )

    player_pitching["double_out"] = np.where(
        player_pitching.at_bat_outcome.isin(double_out_events), 2, np.nan
    )

    player_pitching["triple_out"] = np.where(
        player_pitching.at_bat_outcome.isin(triple_out_events), 3, np.nan
    )

    # Define the number of outs per play
    player_pitching["num_outs_at_bat_outcome"] = (
        player_pitching["single_out"]
        .fillna(player_pitching["double_out"])
        .fillna(player_pitching["triple_out"])
        .fillna(0)
    )

    # Sometimes a batter will get a base hit but a runner is thrown out. We will identify those cases here
    player_pitching["thrown_out"] = [
        1 if " out" in outcome else 0
        for outcome in player_pitching.at_bat_outcome_description
    ]

    # If a runner was thrown out, increase any '0' in num_outs_at_bat_outcome to '1'
    player_pitching["num_outs_at_bat_outcome"] = player_pitching[
        ["num_outs_at_bat_outcome", "thrown_out"]
    ].max(axis=1)

    # Delete intermediate columns
    player_pitching = player_pitching.drop(
        ["single_out", "double_out", "triple_out", "thrown_out"], axis=1
    )

    # Get distinct rows by date, inning_num, and num_outs_at_bat_outcome and sum num_outs_at_bat_outcome.  Divide by 3 to get number of innings
    total_innings_pitched_count = (
        player_pitching[
            ["date", "inning_num", "at_bat_outcome", "num_outs_at_bat_outcome"]
        ].num_outs_at_bat_outcome.sum()
        / 3
    )

    # Average innings per start
    # Of unique games, identify games where a player appeared in the first innning and consider
    # these games "starts"
    min_inning_game = player_pitching.groupby("date")["inning_num"].min().reset_index()
    games_started_dates = min_inning_game[
        min_inning_game.inning_num.astype(int) == 1
    ].date.tolist()
    games_started_count = len(games_started_dates)
    try:
        avg_innings_per_start = (
            player_pitching[
                player_pitching["date"].isin(games_started_dates)
            ].num_outs_at_bat_outcome.sum()
            / 3
            / games_started_count
        )
    except ZeroDivisionError:
        avg_innings_per_start = np.nan

    # Average innings per relief appearance
    games_relief_dates = min_inning_game[
        min_inning_game.inning_num.astype(int) != 1
    ].date.tolist()
    games_relief_count = len(games_relief_dates)
    try:
        avg_innings_per_relief = (
            player_pitching[
                player_pitching["date"].isin(games_relief_dates)
            ].num_outs_at_bat_outcome.sum()
            / 3
            / games_relief_count
        )
    except ZeroDivisionError:
        avg_innings_per_relief = np.nan

    # FIP
    # NOTE the FIP constant is defaulted to 3.1.  The constant is solely
    #   used to bring FIP into the same scale as ERA.

    try:
        hr_count = player_event_counts[
            player_event_counts.at_bat_outcome == "Home Run"
        ].event_counts.sum()
        hbp_bb_count = player_event_counts[
            (player_event_counts.at_bat_outcome == "Hit By Pitch")
            | (player_event_counts.at_bat_outcome == "Walk")
        ].event_counts.sum()
        k_count = player_event_counts[
            player_event_counts.at_bat_outcome == "Strikeout"
        ].event_counts.sum()
        ip = player_pitching[["date", "inning_num"]].drop_duplicates().shape[0]
        player_fip = (13 * hr_count + 3 * hbp_bb_count - 2 * k_count) / ip + 3.1

    except ZeroDivisionError:
        player_fip = np.nan

    # WHIP
    walk_hit_count = player_pitching[
        player_pitching.at_bat_outcome.isin(
            ["Walk", "Intent Walk", "Single", "Double", "Triple", "Home Run"]
        )
    ].event_counts.sum()
    whip = walk_hit_count / total_innings_pitched_count

    # Add all calculations to data frame
    df = pd.DataFrame(
        data=[
            [
                str(mlb_id),
                str(bat_hand),
                total_innings_pitched_count,
                avg_innings_per_start,
                avg_innings_per_relief,
                player_fip,
                whip,
            ]
        ],
        columns=[
            "mlb_id",
            "bat_hand",
            "innings_pitched",
            "avg_innings_per_start",
            "avg_innings_per_relief",
            "fip",
            "whip",
        ],
    )

    return df


# batting average against ---
# on base percentage against ---
# slugging percentage against ---
# Isolated Power (ISO) against ---
# Secondary Average against
# Batting Average Balls in Play against ---
# Strikeout Rate against ---
# Walk Rate against ---
# Homerun rate against ---
# Contact Rate against ---
# Homeruns per flyball against
# Runs Created against

# XXX ERA --> Better represented by FIPS and Run differential XXX
# FIP ---
# WHIP ---
# Avg Innings per game ---


def batting_statistics(mlb_id, pit_hand, start_date, end_date):
    """
    Purpose: Calculates relevant batter statistics for an input timeframe and per a set of input filters

    Inputs:

        mlb_id:
            The mlb id a player or list of players (for team statistics)
            This is a list/tuple of integers

        pit_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string)

    """

    # Generage query strings to input into sql statement
    mlbid_query_string = ", ".join("'" + str(item) + "'" for item in mlb_id)
    pit_hand_query_string = ", ".join("'" + item + "'" for item in pit_hand)

    query = read_sql("pymlb/queries/batting_statistics.sql")
    query = query.format(
        mlbid_query_string, pit_hand_query_string, start_date, end_date
    )
    player_batting = pd.read_sql(query, con)

    if player_batting.shape[0] == 0:
        print("No rows found")
        return pd.DataFrame(
            columns=[
                "mlb_id",
                "pit_hand",
                "hits_count",
                "bb_count",
                "bat_avg",
                "babip",
                "obp",
                "slg",
                "iso",
                "k_rate",
                "bb_rate",
                "bb_k_rate",
                "contact_rate",
            ]
        )

    # Get event counts of player
    player_batting = player_batting.assign(event_counts=1)
    player_event_counts = (
        player_batting[["pit_hand", "at_bat_outcome", "event_counts"]]
        .groupby(["pit_hand", "at_bat_outcome"])
        .count()
        .reset_index()
    )

    # General helper calculations used downstream
    at_bats_events_exclude_list = [
        "Walk",
        "Intent Walk",
        "Hit By Pitch",
        "Sac Fly",
        "Sac Fly DP",
        "Sacrifice Fly",
        "Sacrifice Fly DP",
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
    ]

    plate_appearances_events_exclude_list = [
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
    ]

    player_at_bat_counts = np.sum(
        player_event_counts[
            ~(player_event_counts.at_bat_outcome.isin(at_bats_events_exclude_list))
        ].event_counts
    )
    player_plate_appearance_counts = np.sum(
        player_event_counts[
            ~(
                player_event_counts.at_bat_outcome.isin(
                    plate_appearances_events_exclude_list
                )
            )
        ].event_counts
    )

    # BATTING AVERAGE
    hits_events_list = ["Single", "Double", "Triple", "Home Run"]
    try:
        hits_count = np.sum(
            player_event_counts[
                (player_event_counts.at_bat_outcome.isin(hits_events_list))
            ].event_counts
        )
        player_batting_average = hits_count / player_at_bat_counts
    except ZeroDivisionError:
        hits_count = 0
        player_batting_average = 0

    # BATTING AVERAGE BALLS IN PLAY
    balls_in_play_denominator_exclude_list = [
        "Walk",
        "Intent Walk",
        "Hit By Pitch",
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
        "Strikeout",
        "Home Run",
    ]
    try:
        player_batting_average_balls_in_play = np.sum(
            player_event_counts[
                (
                    player_event_counts.at_bat_outcome.isin(
                        ["Single", "Double", "Triple"]
                    )
                )
            ].event_counts
        ) / np.sum(
            player_event_counts[
                ~(
                    player_event_counts.at_bat_outcome.isin(
                        balls_in_play_denominator_exclude_list
                    )
                )
            ].event_counts
        )
    except ZeroDivisionError:
        player_batting_average_balls_in_play = 0

    # ON BASE PERCENTAGE
    # On Base Percentage: (Hits+Walks+Hit by Pitch) / (At Bats+Walks+Hit by Pitch+Sacrifice Flies
    obp_events_list = [
        "Single",
        "Double",
        "Triple",
        "Home Run",
        "Walk",
        "Hit By Pitch",
        "Intent Walk",
    ]
    try:
        player_on_base_percentage = (
            np.sum(
                player_event_counts[
                    (player_event_counts.at_bat_outcome.isin(obp_events_list))
                ].event_counts
            )
            / player_plate_appearance_counts
        )
    except ZeroDivisionError:
        player_on_base_percentage = np.nan

    # SLUGGING PERCENTAGE
    # Slugging Percentage: ([Singles] + [Doubles x 2] + [Triples x 3] + [Home Runs x 4]) / [At Bats]
    player_singles_count = np.sum(
        player_event_counts[player_event_counts.at_bat_outcome == "Single"].event_counts
    )
    player_doubles_count = np.sum(
        player_event_counts[player_event_counts.at_bat_outcome == "Double"].event_counts
    )
    player_triples_count = np.sum(
        player_event_counts[player_event_counts.at_bat_outcome == "Triple"].event_counts
    )
    player_homerun_count = np.sum(
        player_event_counts[
            player_event_counts.at_bat_outcome == "Home Run"
        ].event_counts
    )

    try:
        player_slugging_percentage = (
            np.sum(
                player_singles_count
                + player_doubles_count * 2
                + player_triples_count * 3
                + player_homerun_count * 4
            )
            / player_at_bat_counts
        )
    except ZeroDivisionError:
        player_slugging_percentage = np.nan

    # ISOLATED POWER
    # ISO: ([Doubles] + [Triples x 2] + [Home Runs x 3]) / [At Bats] = SLG - AVG
    try:
        player_isolated_power = (
            np.sum(
                player_doubles_count
                + player_triples_count * 2
                + player_homerun_count * 3
            )
            / player_at_bat_counts
        )

    except ZeroDivisionError:
        player_isolated_power = np.nan

    # Strikout Rate
    try:
        player_strikout_rate = (
            np.sum(
                player_event_counts[
                    player_event_counts.at_bat_outcome.isin(
                        ["Strikeout", "Strikeout - DP"]
                    )
                ].event_counts
            )
            / player_plate_appearance_counts
        )
    except ZeroDivisionError:
        player_strikout_rate = np.nan

    # Walk Rate
    try:
        bb_count = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Walk"])
            ].event_counts
        )
        player_walk_rate = bb_count / player_plate_appearance_counts

    except ZeroDivisionError:
        bb_count = 0
        player_walk_rate = np.nan

    # Walk to Strikout Ratio
    try:
        player_walk_to_strikeout_ratio = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Walk"])
            ].event_counts
        ) / np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Strikeout", "Strikeout - DP"])
            ].event_counts
        )
    except ZeroDivisionError:
        player_walk_to_strikeout_ratio = np.nan

    # Contact Rate
    contact_events_list = [
        "Single",
        "Double",
        "Triple",
        "Home Run",
        "Groundout",
        "Lineout",
        "Flyout",
    ]
    contact_events_denominator_exclude_list = [
        "Sac Bunt",
        "Sac Bunt DP",
        "Sacrifice Bunt DP",
        "Sacrifice Bunt",
        "Batter Interference",
        "Catcher Interference",
        "Walk",
        "Intent Walk",
        "Hit By Pitch",
    ]

    try:
        player_contact_rate = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(contact_events_list)
            ].event_counts
        ) / np.sum(
            player_event_counts[
                ~(
                    player_event_counts.at_bat_outcome.isin(
                        contact_events_denominator_exclude_list
                    )
                )
            ].event_counts
        )
    except ZeroDivisionError:
        player_contact_rate = np.nan

    # HomeRun Rate
    try:
        player_home_run_rate = (
            np.sum(
                player_event_counts[
                    player_event_counts.at_bat_outcome.isin(["Home Run"])
                ].event_counts
            )
            / player_plate_appearance_counts
        )
    except ZeroDivisionError:
        player_home_run_rate = np.nan

    # HomeRun per flyball Rate
    try:
        player_home_run_per_flyball_rate = np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Home Run"])
            ].event_counts
        ) / np.sum(
            player_event_counts[
                player_event_counts.at_bat_outcome.isin(["Home Run", "Fly ball"])
            ].event_counts
        )
    except ZeroDivisionError:
        player_home_run_per_flyball_rate = np.nan

    # Add all calculations to data frame
    df = pd.DataFrame(
        data=[
            [
                str(mlb_id),
                str(pit_hand),
                hits_count,
                bb_count,
                player_batting_average,
                player_batting_average_balls_in_play,
                player_on_base_percentage,
                player_slugging_percentage,
                player_isolated_power,
                player_strikout_rate,
                player_walk_rate,
                player_walk_to_strikeout_ratio,
                player_contact_rate,
            ]
        ],
        columns=[
            "mlb_id",
            "pit_hand",
            "hits_count",
            "bb_count",
            "bat_avg",
            "babip",
            "obp",
            "slg",
            "iso",
            "k_rate",
            "bb_rate",
            "bb_k_rate",
            "contact_rate",
        ],
    )

    return df

    # batting average ---
    # on base percentage ---
    # slugging percentage ---
    # Isolated Power (ISO) ---
    # Secondary Average
    # Batting Average Balls in Play ---
    # Strikeout Rate ---
    # Walk Rate ---
    # Homerun rate ---
    # Contact Rate ---
    # Homeruns per flyballc ???
    # Runs Created


def map_outcomes_to_contact_types(df):
    """
    Takes as input a dataframe with a column at_bat_outcome_adj. Said column will be remapped to contact type events
    and a pd.DataFrame will be returned

    Input:
        df (pd.DataFrame)

    Output:
        df (pd.DataFrame)

    """

    # Apply new labels
    df.at_bat_outcome_adj = df.at_bat_outcome_adj.map(old_event_to_new_dict)

    # Replace NaN values with at_bat_outcome
    df.at_bat_outcome_adj.fillna(df.at_bat_outcome, inplace=True)

    # Remove irrelevant events
    # Runners getting thrown out, intentional walks, etc. don't provide context on the type of pitcher
    df = df[
        ~(
            df.at_bat_outcome_adj.isin(
                ["Intentional Walk", "Out-Other", "Interference", "Sacrifice"]
            )
        )
    ]

    # Check where general labels can be made more specific and adjust label to groundout, flyout, or lineout appropriately
    df.loc[
        (df.at_bat_outcome_adj == "Forceout")
        & (df.at_bat_outcome_description.str.contains(" ground")),
        "at_bat_outcome_adj",
    ] = "Out-Ground"
    df.loc[
        (df.at_bat_outcome_adj == "Forceout")
        & (df.at_bat_outcome_description.str.contains(" flies")),
        "at_bat_outcome_adj",
    ] = "Out-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Forceout")
        & (df.at_bat_outcome_description.str.contains(" pops")),
        "at_bat_outcome_adj",
    ] = "Out-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Forceout")
        & (df.at_bat_outcome_description.str.contains(" lines")),
        "at_bat_outcome_adj",
    ] = "Out-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Forceout")
        & (df.at_bat_outcome_description.str.contains(" line drive")),
        "at_bat_outcome_adj",
    ] = "Out-Line"

    df.loc[
        (df.at_bat_outcome_adj == "Double Play")
        & (df.at_bat_outcome_description.str.contains(" ground")),
        "at_bat_outcome_adj",
    ] = "Out-Ground"
    df.loc[
        (df.at_bat_outcome_adj == "Double Play")
        & (df.at_bat_outcome_description.str.contains(" flies")),
        "at_bat_outcome_adj",
    ] = "Out-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Double Play")
        & (df.at_bat_outcome_description.str.contains(" pops")),
        "at_bat_outcome_adj",
    ] = "Out-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Double Play")
        & (df.at_bat_outcome_description.str.contains(" lines")),
        "at_bat_outcome_adj",
    ] = "Out-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Double Play")
        & (df.at_bat_outcome_description.str.contains(" line drive")),
        "at_bat_outcome_adj",
    ] = "Out-Line"

    df.loc[
        (df.at_bat_outcome_adj == "Triple Play")
        & (df.at_bat_outcome_description.str.contains(" ground")),
        "at_bat_outcome_adj",
    ] = "Out-Ground"
    df.loc[
        (df.at_bat_outcome_adj == "Triple Play")
        & (df.at_bat_outcome_description.str.contains(" flies")),
        "at_bat_outcome_adj",
    ] = "Out-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Triple Play")
        & (df.at_bat_outcome_description.str.contains(" pops")),
        "at_bat_outcome_adj",
    ] = "Out-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Triple Play")
        & (df.at_bat_outcome_description.str.contains(" lines")),
        "at_bat_outcome_adj",
    ] = "Out-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Triple Play")
        & (df.at_bat_outcome_description.str.contains(" line drive")),
        "at_bat_outcome_adj",
    ] = "Out-Line"

    # NOTE: There are a small number of hits that were also double plays that are counted as "hit-Out in this ins"

    # Categorize hits as linedrive hits, groundball hits, or flyball hits
    df.loc[
        (df.at_bat_outcome_adj == "Single")
        & (df.at_bat_outcome_description.str.contains(" bunt")),
        "at_bat_outcome_adj",
    ] = "Hit-Bunt"
    df.loc[
        (df.at_bat_outcome_adj == "Single")
        & (df.at_bat_outcome_description.str.contains(" ground ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Ground"
    df.loc[
        (df.at_bat_outcome_adj == "Single")
        & (df.at_bat_outcome_description.str.contains(" fly ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Single")
        & (df.at_bat_outcome_description.str.contains(" pop up")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Single")
        & (df.at_bat_outcome_description.str.contains(" lines")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Single")
        & (df.at_bat_outcome_description.str.contains(" line drive")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"

    df.loc[
        (df.at_bat_outcome_adj == "Double")
        & (df.at_bat_outcome_description.str.contains(" bunt")),
        "at_bat_outcome_adj",
    ] = "Hit-Bunt"
    df.loc[
        (df.at_bat_outcome_adj == "Double")
        & (df.at_bat_outcome_description.str.contains(" ground ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Ground"
    df.loc[
        (df.at_bat_outcome_adj == "Double")
        & (df.at_bat_outcome_description.str.contains(" fly ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Double")
        & (df.at_bat_outcome_description.str.contains(" pop up")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Double")
        & (df.at_bat_outcome_description.str.contains(" lines")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Double")
        & (df.at_bat_outcome_description.str.contains(" line drive")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"

    df.loc[
        (df.at_bat_outcome_adj == "Triple")
        & (df.at_bat_outcome_description.str.contains(" bunt")),
        "at_bat_outcome_adj",
    ] = "Hit-Bunt"
    df.loc[
        (df.at_bat_outcome_adj == "Triple")
        & (df.at_bat_outcome_description.str.contains(" ground ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Ground"
    df.loc[
        (df.at_bat_outcome_adj == "Triple")
        & (df.at_bat_outcome_description.str.contains(" fly ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Triple")
        & (df.at_bat_outcome_description.str.contains(" pop up")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Triple")
        & (df.at_bat_outcome_description.str.contains(" lines")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Triple")
        & (df.at_bat_outcome_description.str.contains(" line drive")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"

    df.loc[
        (df.at_bat_outcome_adj == "Home Run")
        & (df.at_bat_outcome_description.str.contains(" ground ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Ground"
    df.loc[
        (df.at_bat_outcome_adj == "Home Run")
        & (df.at_bat_outcome_description.str.contains(" fly ball")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Home Run")
        & (df.at_bat_outcome_description.str.contains(" pop up")),
        "at_bat_outcome_adj",
    ] = "Hit-Fly"
    df.loc[
        (df.at_bat_outcome_adj == "Home Run")
        & (df.at_bat_outcome_description.str.contains(" lines")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Home Run")
        & (df.at_bat_outcome_description.str.contains(" line drive")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Home Run")
        & (df.at_bat_outcome_description.str.contains(" grand slam")),
        "at_bat_outcome_adj",
    ] = "Hit-Line"
    df.loc[
        (df.at_bat_outcome_adj == "Home Run"), "at_bat_outcome_description"
    ] = "Hit-Fly"  # Default remaining homeruns to Hit-Fly (more likely than Hit-Line)

    # Default Remaining unadjusted hits to "Hit-Unknown" and outs to "Out-Unknown"
    df.loc[
        df.at_bat_outcome_adj.isin(["Single", "Double", "Triple", "Home Run"]),
        "at_bat_outcome_adj",
    ] = "Hit-Unknown"
    df.loc[
        df.at_bat_outcome_adj.isin(["Forceout", "Double Play", "Triple Play"]),
        "at_bat_outcome_adj",
    ] = "Out-Unknown"

    # In the way the data is coded, there is no way to know if a fielders choice was a grounder, linedrive, or flyout
    # Assumption: most fielders choices are likely infield gorunders with a forceout to a leading runner
    df.loc[
        (df.at_bat_outcome_adj == "Fielders Choice"), "at_bat_outcome_adj"
    ] = "Out-Ground"

    return df


def batter_contact_type_rates(mlb_id, pit_hand, start_date, end_date):
    """
        Calculates line/ground/fly rates for a player or list of players
        Inputs:

        mlb_id:
            The mlb id a player or list of players (for team statistics)
            This is a list/tuple of integers

        pit_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string)

    """

    mlbid_query_string = ", ".join("'" + str(item) + "'" for item in mlb_id)
    pit_hand_query_string = ", ".join("'" + item + "'" for item in pit_hand)

    query = read_sql("pymlb/queries/batting_statistics.sql")
    query = query.format(
        mlbid_query_string, pit_hand_query_string, start_date, end_date
    )
    player_batting = pd.read_sql(query, con)

    if player_batting.shape[0] == 0:
        return pd.DataFrame()

    # type of hit (grounder, fly ball, line drive, etc.)
    # Adjust event tx to join common outcomes (strikout and strikeout-db together)
    player_batting["at_bat_outcome_adj"] = player_batting["at_bat_outcome"]

    player_batting = map_outcomes_to_contact_types(player_batting)

    if player_batting.shape[0] == 0:
        return pd.DataFrame()

    #
    # BATTER EVENT OUTCOMES #
    #

    outcome_types_counts_batter = (
        player_batting[["at_bat_outcome_adj"]]
        .groupby(["at_bat_outcome_adj"], as_index=False)
        .size()
        .reset_index()
    )
    outcome_types_counts_batter.columns = ["at_bat_outcome_adj", "at_bat_outcome_count"]
    outcome_types_counts_batter_pivot = pd.pivot_table(
        outcome_types_counts_batter,
        values="at_bat_outcome_count",
        columns=["at_bat_outcome_adj"],
    )

    # Replace NaN with 0 counts
    outcome_types_counts_batter_pivot.fillna(0, inplace=True)

    # Get percentages for each type of play
    colTotals = outcome_types_counts_batter_pivot.sum(axis=1)
    outcome_types_percent_batter_pivot = outcome_types_counts_batter_pivot.div(
        colTotals, axis=0
    ).reset_index(drop=True)

    # Replace multiindex with col names
    outcome_types_counts_batter_pivot = outcome_types_counts_batter_pivot.reset_index(
        drop=True
    )
    names = [col for col in outcome_types_percent_batter_pivot.columns.tolist()]
    outcome_types_counts_batter_pivot.columns = names
    outcome_types_percent_batter_pivot.columns = names

    # Make all column names lowercase and replace '-' with '_'
    outcome_types_counts_batter_pivot.columns = outcome_types_counts_batter_pivot.columns.str.lower().str.replace(
        "-", "_"
    )
    outcome_types_percent_batter_pivot.columns = outcome_types_percent_batter_pivot.columns.str.lower().str.replace(
        "-", "_"
    )

    # Add placeholders for columns that do not exist in the database
    # This occurs for data early on in the pitchfx data where an event may not have occurred yet
    cols = [
        "error",
        "hit_bunt",
        "hit_fly",
        "hit_ground",
        "hit_line",
        "hit_unknown",
        "out_fly",
        "out_ground",
        "out_line",
        "out_unknown",
        "strikeout",
        "walk",
    ]

    # format columns
    for counter, colTest in enumerate(cols):

        # Check if colTest exists in outcome_types_counts_batter_pivot and outcome_types_percent_batter_pivot
        # If column doesn't exist, add it in
        if colTest not in outcome_types_counts_batter_pivot:
            outcome_types_counts_batter_pivot.insert(
                loc=counter, column=colTest, value=0
            )
            outcome_types_percent_batter_pivot.insert(
                loc=counter, column=colTest, value=0
            )

        # Add '_count' and '_rate' to each column name
        outcome_types_counts_batter_pivot = outcome_types_counts_batter_pivot.rename(
            columns={colTest: colTest + "_count"}
        )
        outcome_types_percent_batter_pivot = outcome_types_percent_batter_pivot.rename(
            columns={colTest: colTest + "_rate"}
        )

    # Calculate total fly/ground/line
    # Counts
    outcome_types_counts_batter_pivot["fly_count_total"] = (
        outcome_types_counts_batter_pivot["hit_fly_count"]
        + outcome_types_counts_batter_pivot["out_fly_count"]
    )

    outcome_types_counts_batter_pivot["ground_count_total"] = (
        outcome_types_counts_batter_pivot["hit_ground_count"]
        + outcome_types_counts_batter_pivot["out_ground_count"]
    )

    outcome_types_counts_batter_pivot["line_count_total"] = (
        outcome_types_counts_batter_pivot["hit_line_count"]
        + outcome_types_counts_batter_pivot["out_line_count"]
    )

    # Rates
    outcome_types_percent_batter_pivot["fly_rate_total"] = (
        outcome_types_percent_batter_pivot["hit_fly_rate"]
        + outcome_types_percent_batter_pivot["out_fly_rate"]
    )

    outcome_types_percent_batter_pivot["ground_rate_total"] = (
        outcome_types_percent_batter_pivot["hit_ground_rate"]
        + outcome_types_percent_batter_pivot["out_ground_rate"]
    )

    outcome_types_percent_batter_pivot["line_rate_total"] = (
        outcome_types_percent_batter_pivot["hit_line_rate"]
        + outcome_types_percent_batter_pivot["out_line_rate"]
    )

    # Concat counts to rates
    outcome_counts_rates = pd.concat(
        [outcome_types_counts_batter_pivot, outcome_types_percent_batter_pivot], axis=1
    )

    outcome_counts_rates["mlb_id"] = str(mlb_id)
    outcome_counts_rates["pit_hand"] = str(pit_hand)

    return outcome_counts_rates


def pitcher_contact_type_rates_against(mlb_id, bat_hand, start_date, end_date):
    """
        Calculates line/ground/fly rates against a pitcher or list of pitchers

        Inputs:

        mlb_id:
            The mlb id a player or list of players (for team statistics)
            This is a list/tuple of integers. The mlbid of the pitcher stats
            should be calculated for

        bat_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string)
    """
    mlbid_query_string = ", ".join("'" + str(item) + "'" for item in mlb_id)
    bat_hand_query_string = ", ".join("'" + item + "'" for item in bat_hand)

    query = read_sql("pymlb/queries/pitching_statistics.sql")
    query = query.format(
        mlbid_query_string, bat_hand_query_string, start_date, end_date
    )
    player_pitching = pd.read_sql(query, con)

    if player_pitching.shape[0] == 0:
        return pd.DataFrame()

    # type of hit (grounder, fly ball, line drive, etc.)
    # Adjust event tx to join common outcomes (strikout and strikeout-db together)
    player_pitching["at_bat_outcome_adj"] = player_pitching["at_bat_outcome"]

    player_pitching = map_outcomes_to_contact_types(player_pitching)

    if player_pitching.shape[0] == 0:
        return pd.DataFrame()

    #
    # PITCHER EVENT OUTCOMES #
    #

    outcome_types_counts_pitcher = (
        player_pitching[["at_bat_outcome_adj"]]
        .groupby(["at_bat_outcome_adj"], as_index=False)
        .size()
        .reset_index()
    )
    outcome_types_counts_pitcher.columns = [
        "at_bat_outcome_adj",
        "at_bat_outcome_count",
    ]
    outcome_types_counts_pitcher_pivot = pd.pivot_table(
        outcome_types_counts_pitcher,
        values="at_bat_outcome_count",
        columns=["at_bat_outcome_adj"],
    )

    # Replace NaN with 0 counts
    outcome_types_counts_pitcher_pivot.fillna(0, inplace=True)

    # Get percentages for each type of play
    colTotals = outcome_types_counts_pitcher_pivot.sum(axis=1)
    outcome_types_percent_pitcher_pivot = outcome_types_counts_pitcher_pivot.div(
        colTotals, axis=0
    ).reset_index(drop=True)

    # Replace multiindex with col names
    outcome_types_counts_pitcher_pivot = outcome_types_counts_pitcher_pivot.reset_index(
        drop=True
    )
    names = [col for col in outcome_types_percent_pitcher_pivot.columns.tolist()]
    outcome_types_counts_pitcher_pivot.columns = names
    outcome_types_percent_pitcher_pivot.columns = names

    # Make all column names lowercase and replace '-' with '_'
    outcome_types_counts_pitcher_pivot.columns = outcome_types_counts_pitcher_pivot.columns.str.lower().str.replace(
        "-", "_"
    )
    outcome_types_percent_pitcher_pivot.columns = outcome_types_percent_pitcher_pivot.columns.str.lower().str.replace(
        "-", "_"
    )

    # Add placeholders for columns that do not exist in the database
    # This occurs for data early on in the pitchfx data twhere an event may not have occured yet
    cols = [
        "error",
        "hit_bunt",
        "hit_fly",
        "hit_ground",
        "hit_line",
        "hit_unknown",
        "out_fly",
        "out_ground",
        "out_line",
        "out_unknown",
        "strikeout",
        "walk",
    ]

    # format columns
    for counter, colTest in enumerate(cols):

        # Check if colTest exists in outcome_types_counts_pitcher_pivot and outcome_types_percent_pitcher_pivot
        # If column doesn't exist, add it in
        if colTest not in outcome_types_counts_pitcher_pivot:
            outcome_types_counts_pitcher_pivot.insert(
                loc=counter, column=colTest, value=0
            )
            outcome_types_percent_pitcher_pivot.insert(
                loc=counter, column=colTest, value=0
            )

        # Add '_count' and '_rate' to each column name
        outcome_types_counts_pitcher_pivot = outcome_types_counts_pitcher_pivot.rename(
            columns={colTest: "pit_" + colTest + "_count"}
        )
        outcome_types_percent_pitcher_pivot = outcome_types_percent_pitcher_pivot.rename(
            columns={colTest: "pit_" + colTest + "_rate"}
        )

    # Calculate total fly/ground/line
    # Counts
    outcome_types_counts_pitcher_pivot["pit_fly_count_total"] = (
        outcome_types_counts_pitcher_pivot["pit_hit_fly_count"]
        + outcome_types_counts_pitcher_pivot["pit_out_fly_count"]
    )

    outcome_types_counts_pitcher_pivot["pit_ground_count_total"] = (
        outcome_types_counts_pitcher_pivot["pit_hit_ground_count"]
        + outcome_types_counts_pitcher_pivot["pit_out_ground_count"]
    )

    outcome_types_counts_pitcher_pivot["pit_line_count_total"] = (
        outcome_types_counts_pitcher_pivot["pit_hit_line_count"]
        + outcome_types_counts_pitcher_pivot["pit_out_line_count"]
    )

    # Rates
    outcome_types_percent_pitcher_pivot["pit_fly_rate_total"] = (
        outcome_types_percent_pitcher_pivot["pit_hit_fly_rate"]
        + outcome_types_percent_pitcher_pivot["pit_out_fly_rate"]
    )

    outcome_types_percent_pitcher_pivot["pit_ground_rate_total"] = (
        outcome_types_percent_pitcher_pivot["pit_hit_ground_rate"]
        + outcome_types_percent_pitcher_pivot["pit_out_ground_rate"]
    )

    outcome_types_percent_pitcher_pivot["pit_line_rate_total"] = (
        outcome_types_percent_pitcher_pivot["pit_hit_line_rate"]
        + outcome_types_percent_pitcher_pivot["pit_out_line_rate"]
    )

    # Concat counts to rates
    outcome_counts_rates = pd.concat(
        [outcome_types_counts_pitcher_pivot, outcome_types_percent_pitcher_pivot],
        axis=1,
    )

    outcome_counts_rates["mlb_id"] = str(mlb_id)
    outcome_counts_rates["bat_hand"] = str(bat_hand)

    return outcome_counts_rates


def pitching_features(mlb_id, bat_hand, start_date, end_date):
    """
        A wrapper to several other functions to calculate pitching features

        Inputs:

        mlb_id:
            The mlb id a player or list of players (for team statistics)
            This is a list/tuple of integers. The mlbid of the pitcher stats
            should be calculated for

        bat_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string))
    """
    player_pitching_batting_against_statistics = pitching_batting_against_statistics(
        mlb_id, bat_hand, start_date, end_date
    )

    # Get pitch type rates
    player_pitching_statistics = pitching_statistics(
        mlb_id, bat_hand, start_date, end_date
    )

    # Get pitcher event outcome rates
    player_pitcher_contact_type_rates_against = pitcher_contact_type_rates_against(
        mlb_id, bat_hand, start_date, end_date
    )

    # Pitch Type Rates
    player_pitcher_pitch_rate_counts = pitcher_pitch_rate_counts(
        mlb_id, bat_hand, start_date, end_date
    )

    # Concat all datasets together. Merge on mlb_id, bat_hand to ensure validity
    pitcher_features = player_pitching_batting_against_statistics.merge(
        player_pitching_statistics, on=["mlb_id", "bat_hand"]
    )
    pitcher_features = pitcher_features.merge(
        player_pitcher_contact_type_rates_against, on=["mlb_id", "bat_hand"]
    )
    pitcher_features = pd.concat(
        [pitcher_features, player_pitcher_pitch_rate_counts], axis=1
    )

    return pitcher_features


def team_batting_features(mlb_id, pit_hand, start_date, end_date):
    """
        A wrapper to several other functions to calculate batting features

        mlb_id:
            list of player's mlbids (for team statistics)
            This is a list/tuple of integers

        pit_hand:
            List of ['R'], ['L'], or ['R','L'] representing right handed, left handed, and all
            handed batters respectively (string)

        start_date:
            Start date in the form "YYYY-MM-DD" (string)

        end_date:
            End date in the form "YYYY-MM-DD" (string)
    """

    # Iterate through each home team batter
    player_batting_stats_all = []
    player_batting_contact_rates_all = []
    for batter_id in mlb_id:
        # Calculate individual hitting statistics (avg, hr rate, etc.)
        player_batting_stats = batting_statistics(
            mlb_id=[batter_id],
            pit_hand=pit_hand,
            start_date=start_date,
            end_date=end_date,
        )

        player_batting_stats = player_batting_stats.fillna(0)

        player_batting_stats_all.append(player_batting_stats)

        # Get individual player's line drive, flyball, groundball rate (outcome rates_)
        player_batting_contact_rates = batter_contact_type_rates(
            mlb_id=[batter_id],
            pit_hand=pit_hand,
            start_date=start_date,
            end_date=end_date,
        )

        player_batting_contact_rates = player_batting_contact_rates.fillna(0)

        player_batting_contact_rates_all.append(player_batting_contact_rates)

    # Join individual player dfs to one df
    player_batting_stats_all = pd.concat(player_batting_stats_all)
    player_batting_contact_rates_all = pd.concat(player_batting_contact_rates_all)

    # Aggregate individual player status to get a weight team statistics
    team_batting_statistics_bottom_up = player_batting_stats_all.groupby(
        ["pit_hand"], as_index=False
    ).agg(
        {
            "hits_count": "sum",
            "bb_count": "min",
            "bat_avg": "mean",
            "babip": "mean",
            "obp": "mean",
            "slg": "mean",
            "iso": "mean",
            "k_rate": "mean",
            "bb_rate": "mean",
            "contact_rate": "mean",
        }
    )

    team_batting_contact_rates_bottom_up = player_batting_contact_rates_all.groupby(
        ["pit_hand"], as_index=False
    ).agg(
        {
            "error_count": "sum",
            "error_rate": "mean",
            "fly_count_total": "sum",
            "fly_rate_total": "mean",
            "ground_count_total": "sum",
            "ground_rate_total": "mean",
            "hit_bunt_count": "sum",
            "hit_bunt_rate": "mean",
            "hit_fly_count": "sum",
            "hit_fly_rate": "mean",
            "hit_ground_count": "sum",
            "hit_ground_rate": "mean",
            "hit_line_count": "sum",
            "hit_line_rate": "mean",
            "hit_unknown_count": "sum",
            "hit_unknown_rate": "mean",
            "line_count_total": "sum",
            "line_rate_total": "mean",
            "out_fly_count": "sum",
            "out_fly_rate": "mean",
            "out_ground_count": "sum",
            "out_ground_rate": "mean",
            "out_line_count": "sum",
            "out_line_rate": "mean",
            "out_unknown_count": "sum",
            "out_unknown_rate": "mean",
            "strikeout_count": "sum",
            "strikeout_rate": "mean",
            "walk_count": "sum",
            "walk_rate": "mean",
        }
    )

    # Concat feature sets
    output_team_stats = team_batting_statistics_bottom_up.merge(
        team_batting_contact_rates_bottom_up
    )

    return output_team_stats
