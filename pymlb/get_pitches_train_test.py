import pandas as pd
import numpy as np
import sqlite3
import json
import pickle

from tqdm import tqdm
from keras.utils import np_utils
from keras import preprocessing

from sklearn.preprocessing import LabelEncoder
from pymlb.util.database import database_mlb_connection, read_sql
from pymlb.util.data import get_pitches, get_game_logs
from pymlb.util.utils import get_config


def min_max_normalization(data):
    # Scale and standardize features; however leave our unknown value of "na_value" intact
    # data: A numeric pandas.core.series.Series
    data = data.copy()

    # Get the mean of each feature
    mean = data[data != na_value].mean(axis=0)
    std = data[data != na_value].std(axis=0)

    # data -= mean
    data[data != na_value] -= mean

    # x_scaled /= std
    data[data != na_value] /= std

    return data


def add_missing_columns(data, cols, default=0):
    """
    Checks to see if cols exists in data columns.
    For any columns that do not exist in the data, a new column will be created with default
    """

    for col in tqdm(cols):
        if col not in data.columns:
            data[col] = default
            print(f"{col} added!")

    return data


def assign_indices(df, save_directory_path=None):
    """
    Summary:
        Takes as input a dataframe (pitch_clean.pq) and generates mapping dictionaires
        of 'bat_mlbid', 'pit_mlbid', 'umpireHID', and 'parkID'. Returns a tuple of dicts of
        the mapping tables

        This is particularly useful for setting up data for input into an Embedding layer

    Parameters:

        df (DataFrame): A dataframe containing 'bat_mlbid', 'pit_mlbid', 'umpireHID', and 'parkID'

        save_directory_path (str): If not None, the mapping dictionaries will be saved to disc in the directory input here
                                   Include a "/" at the end of the path
    Returns: (tuple[dicts])

    """

    # For all batters, pitchers, umpires, and parks assign a numeric index for use in embedding layers
    # KEY: bat_mlbid, VALUE: index
    batter_index_dict = {}
    idx = 0
    for bat in df.bat_mlbid.unique():
        batter_index_dict[str(bat)] = idx
        idx += 1

    pitcher_index_dict = {}
    idx = 0
    for bat in df.pit_mlbid.unique():
        pitcher_index_dict[str(bat)] = idx
        idx += 1

    ump_index_dict = {}
    idx = 0
    for bat in df.umpireHID.unique():
        ump_index_dict[str(bat)] = idx
        idx += 1

    park_index_dict = {}
    idx = 0
    for bat in df.parkID.unique():
        park_index_dict[str(bat)] = idx
        idx += 1

    # Save to disk
    with open(f"{save_directory_path}/batter_id_index_map.json", "w") as f:
        json.dump(batter_index_dict, f)

    with open(f"{save_directory_path}/pitcher_id_index_map.json", "w") as f:
        json.dump(pitcher_index_dict, f)

    with open(f"{save_directory_path}/ump_id_index_map_m.json", "w") as f:
        json.dump(ump_index_dict, f)

    with open(f"{save_directory_path}/park_id_index_map.json", "w") as f:
        json.dump(park_index_dict, f)

    return batter_index_dict, pitcher_index_dict, ump_index_dict, park_index_dict


def get_x_y(
    pitch, max_atbat_pitches=6, padding_direction="pre", truncating_direction="pre"
):
    """
    Parameters:
        pitch (pd.DataFrame): dataframe of pitches
        start_index: starting index for
        output_prefix: When saving to disc a prefix that wil
    """

    model_features = [
        "at_bat_pitch_counter",
        "pitch_ax",  # acceleration, feet per second, measured at the initial point
        "pitch_ay",  # acceleration, feet per second, measured at the initial point
        "pitch_az",  # acceleration, feet per second, measured at the initial point
        "pitch_break_angle",
        "pitch_break_length",
        "pitch_break_y",
        "pit_L",
        "pit_R",
        "bat_L",
        "bat_R",
        "pitch_type_AB",
        "pitch_type_AS",
        "pitch_type_CH",
        "pitch_type_CU",
        "pitch_type_EP",
        "pitch_type_FA",
        "pitch_type_FC",
        "pitch_type_FF",
        "pitch_type_FO",
        "pitch_type_FS",
        "pitch_type_FT",
        "pitch_type_IN",
        "pitch_type_KC",
        "pitch_type_KN",
        "pitch_type_PO",
        "pitch_type_SC",
        "pitch_type_SI",
        "pitch_type_SL",
        "pitch_type_confidence",
        "pitch_nasty_factor",
        "pitch_spin_dir",
        "pitch_spin_rate",
        "pitch_start_speed",
        "pitch_end_speed",
        "pitch_pfx_x",  # horizontal movement, inches, of the pitch between the release point and home plate, as compared to a theoretical pitch thrown at the same speed with no spin-induced movement. This parameter is measured at y=40 feet regardless of the y0 value.
        "pitch_pfx_z",  # verticle movement, inches, of the pitch between the release point and home plate, as compared to a theoretical pitch thrown at the same speed with no spin-induced movement. This parameter is measured at y=40 feet regardless of the y0 value
        "pitch_px",  # the left/right distance, in feet, of the pitch from the middle of the plate as it crossed home plate. The PITCHf/x coordinate system is oriented to the catcher’s/umpire’s perspective, with distances to the right being positive and to the left being negative.
        "pitch_pz",  # the height of the pitch in feet as it crossed the front of home plate.
        "pitch_vx0",  # Velocity, feet per sec, measured at the initial point
        "pitch_vy0",  # Velocity, feet per sec, measured at the initial point
        "pitch_vz0",  # Velocity, feet per sec, measured at the initial point
        "pitch_x",  # the horizontal location of the pitch as it crossed home plate
        "pitch_x0",  # the left/right distance, in feet, of the pitch, measured at the initial point.
        "pitch_y",  # the vertical location of the pitch as it crossed home plate
        "pitch_y0",  # the distance in feet from home plate where the PITCHf/x system is set to measure the initial parameters.
        "pitch_z0",
    ]  # the height, in feet, of the pitch, measured at the initial point

    # Generate a numeric at-bat key that can be used to distinguish between at bats with the same key in the same inning
    # This can occur if the team bats around and a hitter bats twice in one inning against the same pitcher
    # This effectively gives a unique index for each at bat
    # NOTE / TODO: This will fail if the pitcher has not changed come the second at bat.
    atbat_key_int = []
    prev_atbat_key = None
    counter = 0

    pitch.sort_values(
        ["atbat_key", "inning_num", "at_bat_outcome_description"], inplace=True
    )
    for row_atbat_key in tqdm(pitch.atbat_key):
        if row_atbat_key != prev_atbat_key:
            counter += 1

        atbat_key_int.append(counter)

        prev_atbat_key = row_atbat_key

    pitch["atbat_key_int"] = atbat_key_int
    # pitch.drop('atbat_key', axis = 1, inplace=True)

    # Each group-by represents all the pitches associated with a single at bat
    gb = pitch.groupby("atbat_key_int", as_index=False)
    pitch_gb = [gb.get_group(x) for x in tqdm(gb.groups)]
    atbat_int_keys = []
    pitch_gb_no_key = []
    for atbat in tqdm(pitch_gb):
        # Store at bat outcome to y_atbat
        atbat_int_keys.append(atbat.atbat_key_int.values[0])
        pitch_gb_no_key.append(atbat[model_features])

    # Create dictionaries mapping atbat int key to the outcome
    atbat_key_int_to_outcome_dict = dict(
        zip(pitch.atbat_key_int, pitch.at_bat_outcome_adj_int)
    )
    atbat_key_int_to_outcome_location_dict = dict(
        zip(pitch.atbat_key_int, pitch.atbat_outcome_fielder_location_int)
    )
    atbat_key_int_to_outcome_contact_type_dict = dict(
        zip(pitch.atbat_key_int, pitch.general_contact_types_int)
    )

    # Use atbat_int_keys to get the y atbat outcomes
    # We have to do this because we want just one response row for each at bat.  An atbat can have multiple pitches associated with it
    y_atbat_outcomes = []
    y_locations = []
    y_contact_types = []
    prev_key = None
    for key in tqdm(atbat_int_keys):
        if key != prev_key:
            y_atbat_outcomes.append(atbat_key_int_to_outcome_dict[key])
            y_locations.append(atbat_key_int_to_outcome_location_dict[key])
            y_contact_types.append(atbat_key_int_to_outcome_contact_type_dict[key])
        prev_key = key

    # Convert to numpy arrays
    y_atbat_outcomes = np.array(y_atbat_outcomes)
    y_locations = np.array(y_locations)
    y_contact_types = np.array(y_contact_types)

    x = preprocessing.sequence.pad_sequences(
        pitch_gb_no_key,
        maxlen=max_atbat_pitches,
        dtype=np.float64,
        padding=padding_direction,
        truncating=truncating_direction,
        value=na_value,
    )

    return x, y_atbat_outcomes, y_locations, y_contact_types


if __name__ == "__main__":
    config = get_config()
    na_value = config["data"]["pitches"]["na_value"]

    training_years = list(
        range(
            config["data"]["train"]["start_year"],
            config["data"]["train"]["end_year"] + 1,
        )
    )

    testing_years = list(
        range(
            config["data"]["test"]["start_year"], config["data"]["test"]["end_year"] + 1
        )
    )

    pitch = pd.read_parquet("data/pitch_clean.pq")
    pitch.sort_values(
        [
            "atbat_key",
            "inning_num",
            "at_bat_outcome_description",
            "date",
            "inning_num",
            "at_bat_pitch_counter",
            "home_team",
            "away_team",
        ],
        inplace=True,
    )

    batter_index_dict, pitcher_index_dict, ump_index_dict, park_index_dict = assign_indices(
        pitch, "data/index_maps/"
    )

    # Convert batters/pitchers/umps/parks to indexes of the range [0:nclasses]
    # (Will feed more coherently into np_utils.to_categorical)
    pitch_bat_index_pitch = [batter_index_dict[str(bat)] for bat in pitch.bat_mlbid]
    pitch_pit_index_pitch = [pitcher_index_dict[str(pit)] for pit in pitch.pit_mlbid]
    pitch_ump_index_pitch = [ump_index_dict[str(ump)] for ump in pitch.umpireHID]
    pitch_park_index_pitch = [park_index_dict[str(park)] for park in pitch.parkID]

    # As input into the embedding model, I need an array of indexes corresponding to the bat/pit/ump/park for a given at bat
    # For example, if an at-bat had 10 pitches, I would still only have one bat/pit/ump/park observation for that at bat
    # This amounts to removing sequential duplicates (The rows are grouped by at bat) so something like [1, 1, 1, 2, 2, 3, 3, 1] should yield [1, 2, 3, 1]
    # Batters will not have sequential duplicates; however, pitchers/umps/parks will.  The pit/ump/park will often stay the same at-bat to at-bat.  There should still only be one observation per at bat, though.
    pitch_bat_index_atbat = []
    pitch_pit_index_atbat = []
    pitch_ump_index_atbat = []
    pitch_park_index_atbat = []

    prev_bat = None
    for idx, bat in tqdm(enumerate(pitch.atbat_key), total=len(pitch.atbat_key)):
        if bat != prev_bat:
            pitch_bat_index_atbat.append(pitch_bat_index_pitch[idx])
            pitch_pit_index_atbat.append(pitch_pit_index_pitch[idx])
            pitch_ump_index_atbat.append(pitch_ump_index_pitch[idx])
            pitch_park_index_atbat.append(pitch_park_index_pitch[idx])
        prev_bat = bat

    # Ensure all outputs are the same length
    assert (
        len(pitch_bat_index_atbat)
        == len(pitch_pit_index_atbat)
        == len(pitch_ump_index_atbat)
        == len(pitch_park_index_atbat)
    )

    # Save these
    np.save("data/pitch_bat_index_atbat.npy", pitch_bat_index_atbat)
    np.save("data/pitch_pit_index_atbat.npy", pitch_pit_index_atbat)
    np.save("data/pitch_ump_index_atbat.npy", pitch_ump_index_atbat)
    np.save("data/pitch_park_index_atbat.npy", pitch_park_index_atbat)

    # ONE HOTS
    bat_hand_dummies = pd.get_dummies(pitch.bat_hand)
    pit_hand_dummies = pd.get_dummies(pitch.pit_hand)

    # Pitch type
    pitch_type_dummies = pd.get_dummies("pitch_type_" + pitch.pitch_type)

    pitch = pd.concat(
        [pitch, bat_hand_dummies, pit_hand_dummies, pitch_type_dummies], axis=1
    )
    pitch.fillna(na_value, inplace=True)
    pitch.drop(["pitch_type", "bat_hand", "pit_hand"], axis=1, inplace=True)

    encoder1 = LabelEncoder()
    encoder1.fit(pitch.at_bat_outcome_adj)

    encoder2 = LabelEncoder()
    encoder2.fit(pitch.atbat_outcome_fielder_location)

    encoder3 = LabelEncoder()
    encoder3.fit(pitch.general_contact_types)

    # Assign an index to each outcome
    pitch["at_bat_outcome_adj_int"] = encoder1.transform(pitch.at_bat_outcome_adj)
    pitch["atbat_outcome_fielder_location_int"] = encoder2.transform(
        pitch.atbat_outcome_fielder_location
    )
    pitch["general_contact_types_int"] = encoder3.transform(pitch.general_contact_types)

    # Save a mapping of integer to actual meaning
    # Create dictionaries mapping atbat key to the outcome
    atbat_outcome_adj_int_map = dict(
        zip(pitch.at_bat_outcome_adj_int, pitch.at_bat_outcome_adj)
    )
    atbat_outcome_fielder_location_int_map = dict(
        zip(
            pitch.atbat_outcome_fielder_location_int,
            pitch.atbat_outcome_fielder_location,
        )
    )
    general_contact_types_int_map = dict(
        zip(pitch.general_contact_types_int, pitch.general_contact_types)
    )

    with open("data/y_atbat_outcomes_int_map.json", "w") as f:
        json.dump(atbat_outcome_adj_int_map, f)
    with open("data/y_atbat_outcome_fielder_location_int_map.json", "w") as f:
        json.dump(atbat_outcome_fielder_location_int_map, f)
    with open("data/y_general_contact_types_int_map.json", "w") as f:
        json.dump(general_contact_types_int_map, f)

    # output mappings generated above to disc for storage
    at_bat_outcome_int_map = (
        pitch[["at_bat_outcome_adj", "at_bat_outcome_adj_int"]]
        .drop_duplicates()
        .set_index("at_bat_outcome_adj_int")
        .to_dict()["at_bat_outcome_adj"]
    )
    atbat_outcome_fielder_location_int_map = (
        pitch[["atbat_outcome_fielder_location", "atbat_outcome_fielder_location_int"]]
        .drop_duplicates()
        .set_index("atbat_outcome_fielder_location_int")
        .to_dict()["atbat_outcome_fielder_location"]
    )
    general_contact_types_int_map = (
        pitch[["general_contact_types", "general_contact_types_int"]]
        .drop_duplicates()
        .set_index("general_contact_types_int")
        .to_dict()["general_contact_types"]
    )

    # Save mappings to disc

    # ### Scale and standardize relevant x features
    response_vars = [
        "at_bat_outcome_adj_int",
        "atbat_outcome_fielder_location_int",
        "general_contact_types_int",
        "at_bat_outcome_adj",
        "atbat_outcome_fielder_location",
        "general_contact_types",
        "at_bat_outcome_description",
    ]

    keys = [
        "game_key",
        "atbat_key",
        "date",
        "year",
        "inning_num",
        "home_team",
        "away_team",
        "bat_mlbid",
        "pit_mlbid",
        "umpireHID",
        "parkID",
    ]

    normalize_features = [
        "at_bat_pitch_counter",
        "pitch_ax",  # acceleration, feet per second, measured at the initial point
        "pitch_ay",  # acceleration, feet per second, measured at the initial point
        "pitch_az",  # acceleration, feet per second, measured at the initial point
        "pitch_break_angle",
        "pitch_break_length",
        "pitch_break_y",
        "pitch_type_confidence",
        "pitch_nasty_factor",
        "pitch_spin_dir",
        "pitch_spin_rate",
        "pitch_start_speed",
        "pitch_end_speed",
        "pitch_pfx_x",  # horizontal movement, inches, of the pitch between the release point and home plate, as compared to a theoretical pitch thrown at the same speed with no spin-induced movement. This parameter is measured at y=40 feet regardless of the y0 value.
        "pitch_pfx_z",  # verticle movement, inches, of the pitch between the release point and home plate, as compared to a theoretical pitch thrown at the same speed with no spin-induced movement. This parameter is measured at y=40 feet regardless of the y0 value
        "pitch_px",  # the left/right distance, in feet, of the pitch from the middle of the plate as it crossed home plate. The PITCHf/x coordinate system is oriented to the catcher’s/umpire’s perspective, with distances to the right being positive and to the left being negative.
        "pitch_pz",  # the height of the pitch in feet as it crossed the front of home plate.
        "pitch_vx0",  # Velocity, feet per sec, measured at the initial point
        "pitch_vy0",  # Velocity, feet per sec, measured at the initial point
        "pitch_vz0",  # Velocity, feet per sec, measured at the initial point
        "pitch_x",  # the horizontal location of the pitch as it crossed home plate
        "pitch_x0",  # the left/right distance, in feet, of the pitch, measured at the initial point.
        "pitch_y",  # the vertical location of the pitch as it crossed home plate
        "pitch_y0",  # the distance in feet from home plate where the PITCHf/x system is set to measure the initial parameters.
        "pitch_z0",
    ]
    # tqdm pandas progress initiator
    tqdm.pandas()

    pitch[normalize_features] = pitch[pitch.year.isin(training_years)][
        normalize_features
    ].progress_apply(min_max_normalization, axis=0)
    # pitch = pd.concat([pitch[keys], pitch[response_vars], pitch[non_scale_stand_features], x_scaled], axis=1)

    pitch["umpireHID"] = pitch.umpireHID.astype(str)
    pitch["parkID"] = pitch.parkID.astype(str)
    pitch.to_parquet("data/pitch_clean_standardize.pq")

    # Generate sequence data for LSTM
    # pitch = pd.read_parquet('data/embedding/pitch_clean_standardize.pq')

    pitch = add_missing_columns(
        pitch,
        cols=[
            "pitch_type_AB",
            "pitch_type_AS",
            "pitch_type_CH",
            "pitch_type_CU",
            "pitch_type_EP",
            "pitch_type_FA",
            "pitch_type_FC",
            "pitch_type_FF",
            "pitch_type_FO",
            "pitch_type_FS",
            "pitch_type_FT",
            "pitch_type_IN",
            "pitch_type_KC",
            "pitch_type_KN",
            "pitch_type_PO",
            "pitch_type_SC",
            "pitch_type_SI",
            "pitch_type_SL",
        ],
    )

    pitch_train = pitch[pitch.year.isin(training_years)].copy()
    pitch_test = pitch[pitch.year.isin(testing_years)].copy()

    # NOTE na_value is used as the universal "NA" value
    max_atbat_pitches = config["data"]["pitches"]["at_bat_pitches_max_pitches"]
    padding_direction = config["data"]["pitches"]["padding_direction"]
    truncating_direction = config["data"]["pitches"]["truncating_direction"]

    x_train, y_atbat_outcomes_train, y_locations_train, y_contact_types_train = get_x_y(
        pitch_train, max_atbat_pitches, padding_direction, truncating_direction
    )
    x_test, y_atbat_outcomes_test, y_locations_test, y_contact_types_test = get_x_y(
        pitch_test, max_atbat_pitches, padding_direction, truncating_direction
    )

    # Save train
    np.save("data/train/y_pitch_atbat_outcomes_train.npy", y_atbat_outcomes_train)
    np.save("data/train/y_pitch_locations_train.npy", y_locations_train)
    np.save("data/train/y_pitch_contact_types_train.npy", y_contact_types_train)
    np.save("data/train/x_pitch_train.npy", x_train)

    # Save test
    np.save("data/test/y_pitch_atbat_outcomes_test.npy", y_atbat_outcomes_test)
    np.save("data/test/y_pitch_locations.npy_test", y_locations_test)
    np.save("data/test/y_pitch_contact_types_test.npy", y_contact_types_test)
    np.save("data/test/x_pitch_test.npy", x_test)
