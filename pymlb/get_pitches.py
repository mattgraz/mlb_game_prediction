import pandas as pd
import numpy as np
import sqlite3
import json
from tqdm import tqdm
from keras.utils import np_utils

from sklearn.preprocessing import LabelEncoder
from pymlb.util.database import database_mlb_connection, read_sql
from pymlb.util.data import get_pitches, get_game_logs
from pymlb.util.utils import get_config


if __name__ == "__main__":

    config = get_config()

    training_years = list(
        range(
            config["data"]["train"]["start_year"],
            config["data"]["train"]["end_year"] + 1,
        )
    )
    testing_years = list(
        range(
            config["data"]["test"]["start_year"], config["data"]["test"]["end_year"] + 1
        )
    )
    min_year = np.min(training_years + testing_years)
    max_year = np.max(training_years + testing_years)

    pitch = get_pitches(f"{min_year}-01-01", f"{max_year}-12-31")

    # Read in game logs to pull in Home Umpire ID as well as stadium ID
    gamelogs = get_game_logs(f"{min_year}-01-01", f"{max_year}-12-31")

    # ### Merge in ParkID and UmpID  from gamelogs
    # Create game key
    gamelogs["game_key"] = (
        gamelogs.date.astype(str)
        + gamelogs.homeTeam.str.lower()
        + gamelogs.visitingTeam.str.lower()
    )

    cols = ["game_key", "umpireHID", "parkID"]
    gamelogs = gamelogs[cols]
    # gamelogs.to_csv('data/gamelog_umpire_park.csv', index=False)

    pitch["game_key"] = pitch.atbat_key.str[0:16]

    # merge umpire and park id to pitches
    pitch = pitch.merge(gamelogs, how="left")

    # It appears most games with missing data re exhibition games or games early in the season
    # For a first pass, lets drop these games as they are likely games that just didn't get filtered out appropriately
    # pitch = pitch[pitch.UmpireHID.notnull()].copy()
    # pitch_counts = pitch.groupby('atbat_key', as_index=False)['at_bat_pitch_counter'].count()
    # pitch_counts.sort_values('at_bat_pitch_counter').tail()
    # At bats 2009-08-08phiflo9276376452718 and 2008-09-21cledet4448767460260 are clearly outliers because it is near impossible an at bat would last 312 pitches.
    # We will remove these at bat from our training data
    pitch = pitch[
        ~pitch.atbat_key.isin(
            ["2009-08-08phiflo9276376452718", "2008-09-21cledet4448767460260"]
        )
    ].reset_index(drop=True)

    pitch["date"] = pitch.date.astype(str)
    pitch.to_parquet("data/pitch_clean.pq")
