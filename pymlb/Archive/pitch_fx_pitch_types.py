# PURPOSE: Calculates CAREER counts and percentages of each type of pitch for each pitcher

import pandas as pd
from datetime import datetime, timedelta, date
import concurrent.futures

from pymlb.util.utils import get_update_dates
from pymlb.util.feature import pitcher_rate_counts

# Import Database Connection
from pymlb.util.database import database_mlb_engine, read_sql

engine = database_mlb_engine()

# Import update date range (stored in environment variables exported in makefile)
update_dates_dict = get_update_dates()

UPDATE_START_DATE = datetime.strptime(
    update_dates_dict["UPDATE_START_DATE_PITCHFX"], "%Y-%m-%d"
).date()
UPDATE_END_DATE = datetime.strptime(
    update_dates_dict["UPDATE_END_DATE_PITCHFX"], "%Y-%m-%d"
).date()
UPDATE_DATE_DELTA = UPDATE_END_DATE - UPDATE_START_DATE

# Define list of all new dates to pull data for
UPDATE_DATES = [
    UPDATE_START_DATE + timedelta(days=i) for i in range(1, UPDATE_DATE_DELTA.days + 1)
]
MIN_DATE = [date(2008, 1, 1)] * len(UPDATE_DATES)
# Pitch Rates for all pitchfx data
with concurrent.futures.ProcessPoolExecutor(8) as executor:
    for pitch_rates_dates in executor.map(pitcher_rate_counts, MIN_DATE, UPDATE_DATES):
        # Upload pitch counts and rates for data-through-date to database
        pitch_rates_dates.to_sql(
            name="pitcher_pitch_type_rates", con=engine, if_exists="append", index=False
        )

# Pitch Rates for season
UPDATE_DATES = [
    UPDATE_START_DATE + timedelta(days=i) for i in range(1, UPDATE_DATE_DELTA.days + 1)
]
MIN_DATE = [date(UPDATE_DATE.year, 1, 1) for UPDATE_DATE in UPDATE_DATES]
# Pitch Rates for all pitchfx data
with concurrent.futures.ProcessPoolExecutor(8) as executor:
    for pitch_rates_dates in executor.map(pitcher_rate_counts, MIN_DATE, UPDATE_DATES):
        # Upload pitch counts and rates for data-through-date to database
        pitch_rates_dates.to_sql(
            name="pitcher_pitch_type_rates_season",
            con=engine,
            if_exists="append",
            index=False,
        )


# Pitch Rates for last month
UPDATE_DATES = [
    UPDATE_START_DATE + timedelta(days=i) for i in range(1, UPDATE_DATE_DELTA.days + 1)
]
MIN_DATE = [(UPDATE_DATE - timedelta(days=30)) for UPDATE_DATE in UPDATE_DATES]
# Pitch Rates for all pitchfx data
with concurrent.futures.ProcessPoolExecutor(8) as executor:
    for pitch_rates_dates in executor.map(pitcher_rate_counts, MIN_DATE, UPDATE_DATES):
        # Upload pitch counts and rates for data-through-date to database
        pitch_rates_dates.to_sql(
            name="pitcher_pitch_type_rates_month",
            con=engine,
            if_exists="append",
            index=False,
        )
