""" Purpose: Downloads lahman data.py """

from urllib.request import urlopen
import os
import zipfile
from shutil import rmtree


def download_lahman(url, outdir):
    """
    Purpose: Downloads lahman .csv database and moves files into appropriate directory
    url: String of URL of .zip file to download
    outdir: String of output directory
    """

    file_url = urlopen(url)

    # Check if data/external exists and create if it does not
    try:
        os.stat(outdir)
    except FileNotFoundError:
        os.makedirs(outdir)

    # Download and write file
    with open(outdir + "/lahman.zip", "wb") as file:
        file.write(file_url.read())

    # Unzip file
    with zipfile.ZipFile(outdir + "/lahman.zip") as zip_file:
        zip_file.extractall(outdir)

    # Move lahman database .csv files from the unzipped subdirectory into data/external/lahman
    for name in os.listdir(outdir + "/baseballdatabank-2017.1/core"):
        os.rename(
            outdir + "/baseballdatabank-2017.1/core/{}".format(name),
            outdir + "/{}".format(name),
        )

    # Delete old directories
    rmtree(outdir + "/baseballdatabank-2017.1")

    return 0


if __name__ == "__main__":
    # download lahman data
    RET_CODE = download_lahman(
        "http://seanlahman.com/files/database/baseballdatabank-2017.1.zip",
        "data/external/lahman",
    )
