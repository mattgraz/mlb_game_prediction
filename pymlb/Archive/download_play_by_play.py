""" PURPOSE: Downloads play-by-play data """
from urllib.request import urlopen
import os
import zipfile
import re

import pandas as pd


def download_play_by_play(outdir, years):
    """
    Purpose: Downloads play-by-play data from Retrosheet for a given input year
    """

    # Check if data/external exists and create if it does not
    try:
        os.stat(outdir)
    except FileNotFoundError:
        os.makedirs(outdir)

    # Iterate through each desired year and download game logs
    for year in years:
        file_url = urlopen("http://www.retrosheet.org/events/{}eve.zip".format(year))

        # Download and write file
        with open(outdir + "/play_by_play_{}.zip".format(year), "wb") as file:
            file.write(file_url.read())

        # Unzip file
        with zipfile.ZipFile(outdir + "/play_by_play_{}.zip".format(year)) as zip_file:
            zip_file.extractall(outdir)

        # Delete zip
        os.remove(outdir + "/play_by_play_{}.zip".format(year))

    return 0


if __name__ == "__main__":

    # download game logs data
    YEARS = range(2009, 2016 + 1)
    RET_CODE = download_play_by_play(outdir="data/external/play-by-play", years=YEARS)

    # Call Chadwick to process event files
    os.chdir("data/external/play-by-play/")
    pbp_list = []
    for year in YEARS:
        os.system(
            "cwevent -n -y {} -f 0-96 {}*.EV* > all_pbp_{}.csv".format(year, year, year)
        )
        pbp_year = pd.read_csv("all_pbp_{}.csv".format(year))
        pbp_year["year"] = year
        pbp_list.append(pbp_year)
    pbp_all = pd.concat(pbp_list)

    pbp_all.to_csv("../../interim/play_by_play_all.csv")


# Iterate through each year/team file in data/external/play-by-play load each
# year/team downloaded, rbind rows, output as one dataset
# FIELDS = ['a','a','a','a','a']
# PLAY_BY_PLAY_ALL = pd.DataFrame()
# for filename in os.listdir('data/external/play-by-play'):
#     pbpTeamYear = pd.read_csv('data/external/play-by-play/{}'.format(filename), names=FIELDS)

#    # find year/team in filename
#     pbpTeamYear['year'] = re.findall(r'\d+', filename)[0]
#     pbpTeamYear['team'] = re.findall(r'\D')[0]
#     play_by_play_all = play_by_play_all.append(pbpTeamYear, ignore_index=True)

# PLAY_BY_PLAY_ALL.to_csv('data/external/gamelogs/gamelogs_all.txt')
