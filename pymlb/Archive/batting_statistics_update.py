# For each Cal Date
# For each batter
# calculate statistics for
# Season, T30, T10

import pandas as pd
from datetime import date, datetime, timedelta
import json

from pymlb.util.batting_statistics import *

# Import Database Connection
from pymlb.util.database import database_mlb_engine

engine = database_mlb_engine()

# Import update date range (stored in environment variables exported in makefile)
with open("update_dates_dict.txt", "r") as f:
    update_dates_dict = json.load(f)

UPDATE_START_DATE = datetime.strptime(
    update_dates_dict["UPDATE_START_DATE_BATTER_GAME_DATA"], "%Y-%m-%d"
).date()
UPDATE_END_DATE = datetime.strptime(
    update_dates_dict["UPDATE_END_DATE_BATTER_GAME_DATA"], "%Y-%m-%d"
).date()
UPDATE_DATE_DELTA = UPDATE_END_DATE - UPDATE_START_DATE

# Define list of all new dates to pull data for
UPDATE_DATES = [
    UPDATE_START_DATE + timedelta(days=i) for i in range(1, UPDATE_DATE_DELTA.days + 1)
]

# Iterate through each date and pull data
for UPDATE_DATE in tqdm(UPDATE_DATES):
    print(UPDATE_DATE)

    YEAR = UPDATE_DATE.year
    MONTH = str(UPDATE_DATE.month).zfill(2)
    DAY = str(UPDATE_DATE.day).zfill(2)

    for batter_id in ALL_BATTING_IDS:
        # Season stats
        player_batting_states = batting_statistics(
            mlb_id=batter_id,
            pit_hand=["L"],
            start_date="{}-01-01".format(YEAR),
            end_date=(UPDATE_DATE + timedelta(days=-1)).strftime("%Y-%m-%d"),
        )


player_batting_average
player_batting_average_balls_in_play
player_on_base_percentage
player_slugging_percentage
player_isolated_power
player_strikout_rate
player_walk_rate
player_walk_to_strikeout_ratio
player_contact_rate
