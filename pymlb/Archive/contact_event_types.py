import pandas as pd
import numpy as np

from pymlb.util.utils import old_event_to_new_dict

# Import Database Connection
from pymlb.util.database import database_mlb_connection, read_sql

con = database_mlb_connection()

# Import raw data
bat = pd.read_csv("data/external/pitchFx/edited_atbat_table.csv")

# Create date column
bat["year"] = bat["year"].astype(str)
bat["month"] = bat["month"].astype(str).str.zfill(2)
bat["day"] = bat["day"].astype(str).str.zfill(2)
bat["date"] = pd.to_datetime(bat.year + bat.month + bat.day, format="%Y%m%d")

# Remove unidentified  game types as well as spring training games
# Pitchers generally are practicing their pitches, new mechanics, etc. during spring training
bat = bat[
    (bat.game_type_des != "Unknown") & (bat.game_type_des != "Spring Training")
].cpy()

# type of hit (grounder, fly ball, line drive, etc.)
# Adjust event tx to join common outcomes (strikout and strikeout-db together)
bat["event_tx_adj"] = bat["event_tx"]

# Apply new labels
bat.event_tx_adj = bat.event_tx_adj.map(old_event_to_new_dict)

# Replace NaN values with event_tx
bat.event_tx_adj.fillna(bat.event_tx, inplace=True)

# Remove irrelevant events
# Runners getting thrown out, intentional walks, etc. don't provide context on the type of pitcher
bat = bat[
    ~(
        bat.event_tx_adj.isin(
            ["Intentional Walk", "Out-Other", "Interference", "Sacrifice"]
        )
    )
].copy()

# Check where general labels can be made more specific and adjust label to groundout, flyout, or lineout appropriately
bat.loc[
    (bat.event_tx_adj == "Forceout") & (bat.ab_des.str.contains(" ground")),
    "event_tx_adj",
] = "Out-Ground"
bat.loc[
    (bat.event_tx_adj == "Forceout") & (bat.ab_des.str.contains(" flies")),
    "event_tx_adj",
] = "Out-Fly"
bat.loc[
    (bat.event_tx_adj == "Forceout") & (bat.ab_des.str.contains(" pops")),
    "event_tx_adj",
] = "Out-Fly"
bat.loc[
    (bat.event_tx_adj == "Forceout") & (bat.ab_des.str.contains(" lines")),
    "event_tx_adj",
] = "Out-Line"
bat.loc[
    (bat.event_tx_adj == "Forceout") & (bat.ab_des.str.contains(" line drive")),
    "event_tx_adj",
] = "Out-Line"

bat.loc[
    (bat.event_tx_adj == "Double Play") & (bat.ab_des.str.contains(" ground")),
    "event_tx_adj",
] = "Out-Ground"
bat.loc[
    (bat.event_tx_adj == "Double Play") & (bat.ab_des.str.contains(" flies")),
    "event_tx_adj",
] = "Out-Fly"
bat.loc[
    (bat.event_tx_adj == "Double Play") & (bat.ab_des.str.contains(" pops")),
    "event_tx_adj",
] = "Out-Fly"
bat.loc[
    (bat.event_tx_adj == "Double Play") & (bat.ab_des.str.contains(" lines")),
    "event_tx_adj",
] = "Out-Line"
bat.loc[
    (bat.event_tx_adj == "Double Play") & (bat.ab_des.str.contains(" line drive")),
    "event_tx_adj",
] = "Out-Line"

bat.loc[
    (bat.event_tx_adj == "Triple Play") & (bat.ab_des.str.contains(" ground")),
    "event_tx_adj",
] = "Out-Ground"
bat.loc[
    (bat.event_tx_adj == "Triple Play") & (bat.ab_des.str.contains(" flies")),
    "event_tx_adj",
] = "Out-Fly"
bat.loc[
    (bat.event_tx_adj == "Triple Play") & (bat.ab_des.str.contains(" pops")),
    "event_tx_adj",
] = "Out-Fly"
bat.loc[
    (bat.event_tx_adj == "Triple Play") & (bat.ab_des.str.contains(" lines")),
    "event_tx_adj",
] = "Out-Line"
bat.loc[
    (bat.event_tx_adj == "Triple Play") & (bat.ab_des.str.contains(" line drive")),
    "event_tx_adj",
] = "Out-Line"

# NOTE: There are a small number of hits that were also double plays that are counted as "hit-Out in this ins"

# Categorize hits as linedrive hits, groundball hits, or flyball hits
bat.loc[
    (bat.event_tx_adj == "Single") & (bat.ab_des.str.contains(" bunt")), "event_tx_adj"
] = "Hit-Bunt"
bat.loc[
    (bat.event_tx_adj == "Single") & (bat.ab_des.str.contains(" ground ball")),
    "event_tx_adj",
] = "Hit-Ground"
bat.loc[
    (bat.event_tx_adj == "Single") & (bat.ab_des.str.contains(" fly ball")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Single") & (bat.ab_des.str.contains(" pop up")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Single") & (bat.ab_des.str.contains(" lines")), "event_tx_adj"
] = "Hit-Line"
bat.loc[
    (bat.event_tx_adj == "Single") & (bat.ab_des.str.contains(" line drive")),
    "event_tx_adj",
] = "Hit-Line"

bat.loc[
    (bat.event_tx_adj == "Double") & (bat.ab_des.str.contains(" bunt")), "event_tx_adj"
] = "Hit-Bunt"
bat.loc[
    (bat.event_tx_adj == "Double") & (bat.ab_des.str.contains(" ground ball")),
    "event_tx_adj",
] = "Hit-Ground"
bat.loc[
    (bat.event_tx_adj == "Double") & (bat.ab_des.str.contains(" fly ball")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Double") & (bat.ab_des.str.contains(" pop up")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Double") & (bat.ab_des.str.contains(" lines")), "event_tx_adj"
] = "Hit-Line"
bat.loc[
    (bat.event_tx_adj == "Double") & (bat.ab_des.str.contains(" line drive")),
    "event_tx_adj",
] = "Hit-Line"

bat.loc[
    (bat.event_tx_adj == "Triple") & (bat.ab_des.str.contains(" bunt")), "event_tx_adj"
] = "Hit-Bunt"
bat.loc[
    (bat.event_tx_adj == "Triple") & (bat.ab_des.str.contains(" ground ball")),
    "event_tx_adj",
] = "Hit-Ground"
bat.loc[
    (bat.event_tx_adj == "Triple") & (bat.ab_des.str.contains(" fly ball")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Triple") & (bat.ab_des.str.contains(" pop up")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Triple") & (bat.ab_des.str.contains(" lines")), "event_tx_adj"
] = "Hit-Line"
bat.loc[
    (bat.event_tx_adj == "Triple") & (bat.ab_des.str.contains(" line drive")),
    "event_tx_adj",
] = "Hit-Line"

bat.loc[
    (bat.event_tx_adj == "Home Run") & (bat.ab_des.str.contains(" ground ball")),
    "event_tx_adj",
] = "Hit-Ground"
bat.loc[
    (bat.event_tx_adj == "Home Run") & (bat.ab_des.str.contains(" fly ball")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Home Run") & (bat.ab_des.str.contains(" pop up")),
    "event_tx_adj",
] = "Hit-Fly"
bat.loc[
    (bat.event_tx_adj == "Home Run") & (bat.ab_des.str.contains(" lines")),
    "event_tx_adj",
] = "Hit-Line"
bat.loc[
    (bat.event_tx_adj == "Home Run") & (bat.ab_des.str.contains(" line drive")),
    "event_tx_adj",
] = "Hit-Line"
bat.loc[
    (bat.event_tx_adj == "Home Run") & (bat.ab_des.str.contains(" grand slam")),
    "event_tx_adj",
] = "Hit-Line"
bat.loc[
    (bat.event_tx_adj == "Home Run"), "ab_des"
] = "Hit-Fly"  # Default remaining homeruns to Hit-Fly (more likely than Hit-Line)

# Default Remaining unadjusted hits to "Hit-Unknown" and outs to "Out-Unknown"
bat.loc[
    bat.event_tx_adj.isin(["Single", "Double", "Triple", "Home Run"]), "event_tx_adj"
] = "Hit-Unknown"
bat.loc[
    bat.event_tx_adj.isin(["Forceout", "Double Play", "Triple Play"]), "event_tx_adj"
] = "Out-Unknown"

# In the way the data is coded, there is no way to know if a fielders choice was a grounder, linedrive, or flyout
# Assumption: most fielders choices are likely infield gorunders with a forceout to a leading runner
bat.loc[(bat.event_tx_adj == "Fielders Choice"), "event_tx_adj"] = "Out-Ground"

### ### ### ### ### ### ### ####
#### PITCHER EVENT OUTCOMES ####
### ### ### ### ### ### ### ####

# Iterate through each day calculating rolling statistics
for day in bat.date.unique():
    print(day)
    bat_date_filter = bat[bat.date <= day].copy()

    outcome_types_counts_pitcher = (
        bat_date_filter[["pit_mlbid", "event_tx_adj", "pit_hand_cd", "bat_hand_cd"]]
        .groupby(
            ["pit_mlbid", "event_tx_adj", "pit_hand_cd", "bat_hand_cd"], as_index=False
        )
        .size()
        .reset_index()
    )
    outcome_types_counts_pitcher.columns = [
        "pit_mlbid",
        "event_tx_adj",
        "pit_hand_cd",
        "bat_hand_cd",
        "event_tx_count",
    ]
    outcome_types_counts_pitcher_pivot = pd.pivot_table(
        outcome_types_counts_pitcher,
        values="event_tx_count",
        index=["pit_mlbid", "pit_hand_cd", "bat_hand_cd"],
        columns=["event_tx_adj"],
    )

    # Replace NaN with 0 counts
    outcome_types_counts_pitcher_pivot.fillna(0, inplace=True)

    # Get percentages for each type of play
    colTotals = outcome_types_counts_pitcher_pivot.sum(axis=1)
    outcome_types_percent_pitcher_pivot = outcome_types_counts_pitcher_pivot.div(
        colTotals, axis=0
    ).reset_index()

    # Replace multiindex with col names
    outcome_types_counts_pitcher_pivot = (
        outcome_types_counts_pitcher_pivot.reset_index()
    )
    names = [col for col in outcome_types_percent_pitcher_pivot.columns.tolist()]
    outcome_types_counts_pitcher_pivot.columns = names
    outcome_types_percent_pitcher_pivot.columns = names

    # Output Data
    outcome_types_counts_pitcher_pivot.to_csv(
        "data/interim/pitcher_outcome_type_counts.csv"
    )
    outcome_types_percent_pitcher_pivot.to_csv(
        "data/interim/pitcher_outcome_type_percents.csv"
    )

    ### ### ### ### ### ### ### ###
    #### BATTER EVENT OUTCOMES ####
    ### ### ### ### ### ### ### ###

    outcome_types_counts_batter = (
        bat_date_filter[["bat_mlbid", "event_tx_adj", "pit_hand_cd", "bat_hand_cd"]]
        .groupby(
            ["bat_mlbid", "event_tx_adj", "pit_hand_cd", "bat_hand_cd"], as_index=False
        )
        .size()
        .reset_index()
    )
    outcome_types_counts_batter.columns = [
        "bat_mlbid",
        "event_tx_adj",
        "pit_hand_cd",
        "bat_hand_cd",
        "event_tx_count",
    ]
    outcome_types_counts_batter_pivot = pd.pivot_table(
        outcome_types_counts_batter,
        values="event_tx_count",
        index=["bat_mlbid", "pit_hand_cd", "bat_hand_cd"],
        columns=["event_tx_adj"],
    )

    # Replace NaN with 0 counts
    outcome_types_counts_batter_pivot.fillna(0, inplace=True)

    # Get percentages for each type of play
    colTotals = outcome_types_counts_batter_pivot.sum(axis=1)
    outcome_types_percent_batter_pivot = outcome_types_counts_batter_pivot.div(
        colTotals, axis=0
    ).reset_index()

    # Replace multiindex with col names
    outcome_types_counts_batter_pivot = outcome_types_counts_batter_pivot.reset_index()
    names = [col for col in outcome_types_percent_batter_pivot.columns.tolist()]
    outcome_types_counts_batter_pivot.columns = names
    outcome_types_percent_batter_pivot.columns = names

    outcome_types_counts_batter_pivot.insert(
        loc=0, column="data_through_date", value=day
    )
    outcome_types_percent_batter_pivot.insert(
        loc=0, column="data_through_date", value=day
    )

    # Make all column names lowercase and replace '-' with '_'
    outcome_types_counts_batter_pivot.columns = outcome_types_counts_batter_pivot.columns.str.lower().str.replace(
        "-", "_"
    )
    outcome_types_percent_batter_pivot.columns = outcome_types_percent_batter_pivot.columns.str.lower().str.replace(
        "-", "_"
    )

    # Add placeholders for columns that do not exist in the database
    # This occurs for data early on in the pitchfx data twhere an event may not have occured yet
    cols = [
        "data_through_date",
        "bat_mlbid",
        "bat_hand_cd",
        "pit_hand_cd",
        "error",
        "hit_bunt",
        "hit_fly",
        "hit_ground",
        "hit_line",
        "hit_unknown",
        "out_fly",
        "out_ground",
        "out_line",
        "out_unknown",
        "strikeout",
        "walk",
    ]

    for counter, colTest in enumerate(cols):

        # Check if colTest exists in outcome_types_counts_batter_pivot and outcome_types_percent_batter_pivot
        # If column doesn't exist, add it in
        if colTest not in outcome_types_counts_batter_pivot:
            outcome_types_counts_batter_pivot.insert(
                loc=counter, column=colTest, value=np.nan
            )
            outcome_types_percent_batter_pivot.insert(
                loc=counter, column=colTest, value=np.nan
            )

    # Upload data to database
    print("Starting database upload")
    outcome_types_counts_batter_pivot[cols].to_sql(
        name="batter_outcome_type_counts", con=engine, if_exists="append", index=False
    )

    outcome_types_percent_batter_pivot[cols].to_sql(
        name="batter_outcome_type_percent", con=engine, if_exists="append", index=False
    )
