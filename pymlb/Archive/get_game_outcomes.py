"""
Purpose: Downloads team info and game info for a given day
Ex: team record, game start time, venue, game outcomes, etc.
"""

from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import URLError
import re
import os
from datetime import datetime, timedelta
import pandas as pd
import json

# Import Database Connection
from pymlb.util.database import database_mlb_engine

engine = database_mlb_engine()

with open("update_dates_dict.txt", "r") as f:
    update_dates_dict = json.load(f)

# Import update date range (stored in environment variables exported in makefile)
UPDATE_START_DATE = datetime.strptime(
    update_dates_dict["UPDATE_START_DATE_GAME_OUTCOMES"], "%Y-%m-%d"
).date()
UPDATE_END_DATE = datetime.strptime(
    update_dates_dict["UPDATE_END_DATE_GAME_OUTCOMES"], "%Y-%m-%d"
).date()
UPDATE_DATE_DELTA = UPDATE_END_DATE - UPDATE_START_DATE

# Define list of all new dates to pull data for
UPDATE_DATES = [
    UPDATE_START_DATE + timedelta(days=i) for i in range(1, UPDATE_DATE_DELTA.days + 1)
]

# Define base URL to add onto to pull data
BASE_URL = "http://gd2.mlb.com/components/game/mlb"

with open("data/game_outcomes.csv", "a+", encoding="utf-8") as game_outcomes_file:
    # Iterate through each date and pull data
    for UPDATE_DATE in UPDATE_DATES:
        print(UPDATE_DATE)

        # Define update date URL
        GAME_DATE_URL = BASE_URL + "/year_{}/month_{}/day_{}/epg.xml".format(
            UPDATE_DATE.year,
            str(UPDATE_DATE.month).zfill(2),
            str(UPDATE_DATE.day).zfill(2),
        )
        try:
            team_file_soup = BeautifulSoup(urlopen(GAME_DATE_URL), "lxml")
        except URLError:
            print(
                "No Games on {}-{}-{}".format(
                    UPDATE_DATE.year,
                    str(UPDATE_DATE.month).zfill(2),
                    str(UPDATE_DATE.day).zfill(2),
                )
            )
            continue

        # Iterate through each game on a given day and download data
        for team_data in team_file_soup.epg.find_all("game"):
            game_outcomes_file.write(
                "{}-{}-{}|".format(
                    UPDATE_DATE.year,
                    str(UPDATE_DATE.month).zfill(2),
                    str(UPDATE_DATE.day).zfill(2),
                )
            )
            game_outcomes_file.write(team_data.get("calendar_event_id", "") + "|")
            game_outcomes_file.write(team_data.get("start", "") + "|")
            game_outcomes_file.write(team_data.get("id", "") + "|")
            game_outcomes_file.write(team_data.get("venue", "") + "|")
            game_outcomes_file.write(team_data.get("game_pk", "") + "|")
            game_outcomes_file.write(team_data.get("time_date", "") + "|")
            game_outcomes_file.write(team_data.get("time_date_aw_lg", "") + "|")
            game_outcomes_file.write(team_data.get("time_date_hm_lg", "") + "|")
            game_outcomes_file.write(team_data.get("time_zone", "") + "|")
            game_outcomes_file.write(team_data.get("ampm", "") + "|")
            game_outcomes_file.write(team_data.get("first_pitch_et", "") + "|")
            game_outcomes_file.write(team_data.get("away_time", "") + "|")
            game_outcomes_file.write(team_data.get("away_time_zone", "") + "|")
            game_outcomes_file.write(team_data.get("away_ampm", "") + "|")
            game_outcomes_file.write(team_data.get("home_time", "") + "|")
            game_outcomes_file.write(team_data.get("home_time_zone", "") + "|")
            game_outcomes_file.write(team_data.get("home_ampm", "") + "|")
            game_outcomes_file.write(team_data.get("game_type", "") + "|")
            game_outcomes_file.write(team_data.get("tiebreaker_sw", "") + "|")
            game_outcomes_file.write(team_data.get("resume_date", "") + "|")
            game_outcomes_file.write(team_data.get("original_date", "") + "|")
            game_outcomes_file.write(team_data.get("time_zone_aw_lg", "") + "|")
            game_outcomes_file.write(team_data.get("time_zone_hm_lg", "") + "|")
            game_outcomes_file.write(team_data.get("time_aw_lg", "") + "|")
            game_outcomes_file.write(team_data.get("aw_lg_ampm", "") + "|")
            game_outcomes_file.write(team_data.get("tz_aw_lg_gen", "") + "|")
            game_outcomes_file.write(team_data.get("time_hm_lg", "") + "|")
            game_outcomes_file.write(team_data.get("hm_lg_ampm", "") + "|")
            game_outcomes_file.write(team_data.get("tz_hm_lg_gen", "") + "|")
            game_outcomes_file.write(team_data.get("venue_id", "") + "|")
            game_outcomes_file.write(team_data.get("scheduled_innings", "") + "|")
            game_outcomes_file.write(team_data.get("description", "") + "|")
            game_outcomes_file.write(team_data.get("away_name_abbrev", "") + "|")
            game_outcomes_file.write(team_data.get("home_name_abbrev", "") + "|")
            game_outcomes_file.write(team_data.get("away_code", "") + "|")
            game_outcomes_file.write(team_data.get("away_file_code", "") + "|")
            game_outcomes_file.write(team_data.get("away_team_id", "") + "|")
            game_outcomes_file.write(team_data.get("away_team_city", "") + "|")
            game_outcomes_file.write(team_data.get("away_team_name", "") + "|")
            game_outcomes_file.write(team_data.get("away_division", "") + "|")
            game_outcomes_file.write(team_data.get("away_league_id", "") + "|")
            game_outcomes_file.write(team_data.get("away_sport_code", "") + "|")
            game_outcomes_file.write(team_data.get("home_code", "") + "|")
            game_outcomes_file.write(team_data.get("home_file_code", "") + "|")
            game_outcomes_file.write(team_data.get("home_team_id", "") + "|")
            game_outcomes_file.write(team_data.get("home_team_city", "") + "|")
            game_outcomes_file.write(team_data.get("home_team_name", "") + "|")
            game_outcomes_file.write(team_data.get("home_division", "") + "|")
            game_outcomes_file.write(team_data.get("home_league_id", "") + "|")
            game_outcomes_file.write(team_data.get("home_sport_code", "") + "|")
            game_outcomes_file.write(team_data.get("day", "") + "|")
            game_outcomes_file.write(team_data.get("gameday_sw", "") + "|")
            game_outcomes_file.write(team_data.get("double_header_sw", "") + "|")
            game_outcomes_file.write(team_data.get("game_nbr", "") + "|")
            game_outcomes_file.write(team_data.get("tbd_flag", "") + "|")
            game_outcomes_file.write(team_data.get("away_games_back", "") + "|")
            game_outcomes_file.write(team_data.get("home_games_back", "") + "|")
            game_outcomes_file.write(
                team_data.get("away_games_back_wildcard", "") + "|"
            )
            game_outcomes_file.write(
                team_data.get("home_games_back_wildcard", "") + "|"
            )
            game_outcomes_file.write(team_data.get("venue_w_chan_loc", "") + "|")
            game_outcomes_file.write(team_data.get("location", "") + "|")
            game_outcomes_file.write(team_data.get("gameday", "") + "|")
            game_outcomes_file.write(team_data.get("away_win", "") + "|")
            game_outcomes_file.write(team_data.get("away_loss", "") + "|")
            game_outcomes_file.write(team_data.get("home_win", "") + "|")
            game_outcomes_file.write(team_data.get("home_loss", "") + "|")
            game_outcomes_file.write(team_data.get("game_data_directory", "") + "|")
            game_outcomes_file.write(team_data.get("time", "") + "|")
            game_outcomes_file.write(team_data.get("top_inning", "") + "|")
            game_outcomes_file.write(team_data.get("status", "") + "|")
            game_outcomes_file.write(team_data.get("ind", "") + "|")
            game_outcomes_file.write(team_data.get("inning", "") + "|")
            game_outcomes_file.write(team_data.get("outs", "") + "|")
            game_outcomes_file.write(team_data.get("away_team_runs", "") + "|")
            game_outcomes_file.write(team_data.get("home_team_runs", "") + "|")
            game_outcomes_file.write(team_data.get("away_team_hits", "") + "|")
            game_outcomes_file.write(team_data.get("home_team_hits", "") + "|")
            game_outcomes_file.write(team_data.get("away_team_errors", "") + "|")
            game_outcomes_file.write(team_data.get("home_team_errors", ""))
            game_outcomes_file.write("\n")

# Read in the file just output and upload to the database
## ISSUE: This is hacky right now, needs refactoring
cols = [
    "game_date",
    "calendar_event_id",
    "start",
    "id",
    "venue",
    "game_pk",
    "time_date",
    "time_date_aw_lg",
    "time_date_hm_lg",
    "time_zone",
    "ampm",
    "first_pitch_et",
    "away_time",
    "away_time_zone",
    "away_ampm",
    "home_time",
    "home_time_zone",
    "home_ampm",
    "game_type",
    "tiebreaker_sw",
    "resume_date",
    "original_date",
    "time_zone_aw_lg",
    "time_zone_hm_lg",
    "time_aw_lg",
    "aw_lg_ampm",
    "tz_aw_lg_gen",
    "time_hm_lg",
    "hm_lg_ampm",
    "tz_hm_lg_gen",
    "venue_id",
    "scheduled_innings",
    "description",
    "away_name_abbrev",
    "home_name_abbrev",
    "away_code",
    "away_file_code",
    "away_team_id",
    "away_team_city",
    "away_team_name",
    "away_division",
    "away_league_id",
    "away_sport_code",
    "home_code",
    "home_file_code",
    "home_team_id",
    "home_team_city",
    "home_team_name",
    "home_division",
    "home_league_id",
    "home_sport_code",
    "day",
    "gameday_sw",
    "double_header_sw",
    "game_nbr",
    "tbd_flag",
    "away_games_back",
    "home_games_back",
    "away_games_back_wildcard",
    "home_games_back_wildcard",
    "venue_w_chan_loc",
    "location",
    "gameday",
    "away_win",
    "away_loss",
    "home_win",
    "home_loss",
    "game_data_directory",
    "time",
    "top_inning",
    "status",
    "ind",
    "inning",
    "outs",
    "away_team_runs",
    "home_team_runs",
    "away_team_hits",
    "home_team_hits",
    "away_team_errors",
    "home_team_errors",
]

game_outcomes = pd.read_table(
    "data/game_outcomes.csv", names=cols, index_col=False, sep="|", low_memory=False
)

game_outcomes.to_sql(name="game_outcomes", con=engine, if_exists="append", index=False)
