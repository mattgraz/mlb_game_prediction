import re
from bs4 import BeautifulSoup, UnicodeDammit
import os
from datetime import datetime, timedelta
import time
import json
import pandas as pd
from time import sleep
import concurrent.futures
from tqdm import tqdm

from pymlb.util.web import urlopen_robust, scrape_pitchfx_game

# Import Database Connection
from pymlb.util.database import database_mlb_engine

engine = database_mlb_engine()

# Import update date range (stored in environment variables exported in makefile)
with open("update_dates_dict.txt", "r") as f:
    update_dates_dict = json.load(f)

UPDATE_START_DATE = datetime.strptime(
    update_dates_dict["UPDATE_START_DATE_PITCHFX"], "%Y-%m-%d"
).date()
UPDATE_END_DATE = datetime.strptime(
    update_dates_dict["UPDATE_END_DATE_PITCHFX"], "%Y-%m-%d"
).date()

base_url = "http://gd2.mlb.com/components/game/mlb/"

UPDATE_DATE_DELTA = UPDATE_END_DATE - UPDATE_START_DATE

# Define list of all new dates to pull data for
UPDATE_DATES = [
    UPDATE_START_DATE + timedelta(days=i) for i in range(1, UPDATE_DATE_DELTA.days + 1)
]

cols = [
    "date",
    "home_team",
    "away_team",
    "inning_num",
    "bat_mlbid",
    "pit_mlbid",
    "bat_hand",
    "pit_hand",
    "away_team_runs",
    "home_team_runs",
    "at_bat_outcome",
    "at_bat_outcome_description",
    "at_bat_pitch_counter",
    "pitch_ax",
    "pitch_ay",
    "pitch_az",
    "pitch_break_angle",
    "pitch_break_length",
    "pitch_break_y",
    "pitch_batting_count_type",
    "pitch_type",
    "pitch_type_confidence",
    "pitch_outcome_description",
    "pitch_nasty_factor",
    "pitch_pfx_x",
    "pitch_pfx_z",
    "pitch_px",
    "pitch_pz",
    "pitch_spin_dir",
    "pitch_spin_rate",
    "pitch_start_speed",
    "pitch_end_speed",
    "pitch_vx0",
    "pitch_vy0",
    "pitch_vz0",
    "pitch_x",
    "pitch_x0",
    "pitch_y",
    "pitch_y0",
    "pitch_z0",
    "pitch_zone",
    "pitch_time_zulu",
]

executorT = concurrent.futures.ThreadPoolExecutor(15)
executorP = concurrent.futures.ProcessPoolExecutor(8)


def check_for_all_innings_file(game_url):
    """
    Takes as input a url.  Checks to see if "innings_all.xml" exists and returns the url string to the file location if it does
    """
    game_url_soup = BeautifulSoup(urlopen_robust(game_url), "lxml").find_all(
        "a", href=re.compile("inning")
    )
    if len(game_url_soup) > 0:
        game_inning_url = game_url + "inning/"

        innings_all_inds = BeautifulSoup(
            urlopen_robust(game_inning_url), "lxml"
        ).find_all("a", href=re.compile("inning_all"))

        if len(innings_all_inds) > 0:
            game_inning_all_url = game_inning_url + "inning_all.xml"
        else:
            return ""
    else:
        return ""

    return game_inning_all_url


# Iterate through each date and pull data
for UPDATE_DATE in tqdm(UPDATE_DATES):
    print(UPDATE_DATE)

    date_month_url = base_url + "year_{}/month_{}".format(
        str(UPDATE_DATE.year), str(UPDATE_DATE.month).zfill(2)
    )

    date_month_url_soup = BeautifulSoup(urlopen_robust(date_month_url), "lxml")
    date_month_url_soup = str(
        date_month_url_soup.find_all("a", href=re.compile("month"))
    )

    if not (
        "month_{}/day_{}".format(
            str(UPDATE_DATE.month).zfill(2), str(UPDATE_DATE.day).zfill(2)
        )
        in date_month_url_soup
    ):
        print("no data")
        continue

    date_url = base_url + "year_{}/month_{}/day_{}".format(
        str(UPDATE_DATE.year),
        str(UPDATE_DATE.month).zfill(2),
        str(UPDATE_DATE.day).zfill(2),
    )

    # Collect games on the date
    date_url_soup = BeautifulSoup(urlopen_robust(date_url), "lxml")

    # Iterate through each game
    game_urls = [
        date_url + "/" + game.get_text().strip()
        for game in date_url_soup.find_all("a", href=re.compile("gid_*"))
    ]

    # # Check if innings directory exits (it will not exist if the game wasn't actually played)
    # inning_folder_inds = [BeautifulSoup(urlopen_robust(game), 'lxml').find_all('a', href=re.compile('inning')) for game in game_urls]
    # game_inning_urls = [game + 'inning/' for game, inning_folder_ind in zip(game_urls, inning_folder_inds) if len(inning_folder_ind) > 0]

    # # Check for Inning_all.xml file (Spring training will not have these and early pitch fx)
    # innings_all_inds = [BeautifulSoup(urlopen_robust(game), 'lxml').find_all('a', href=re.compile('inning_all')) for game in game_inning_urls]
    # game_inning_urls_innings_all = [game + 'inning_all.xml' for game, inning_folder_ind in zip(game_inning_urls, innings_all_inds) if len(inning_folder_ind) > 0]
    game_inning_urls_innings_all = []
    for test_url in executorT.map(check_for_all_innings_file, game_urls):
        if test_url != "":
            game_inning_urls_innings_all.append(test_url)

    # Define empty dataframe to append new pitch rows to
    game_df = pd.DataFrame(columns=cols)

    for new_row in executorP.map(
        scrape_pitchfx_game,
        game_inning_urls_innings_all,
        [UPDATE_DATE] * len(game_inning_urls_innings_all),
    ):
        game_df = game_df.append(new_row)

    # Upload to database
    game_df.to_sql(name="pitchFx", con=engine, if_exists="append", index=False)
