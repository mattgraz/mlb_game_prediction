# Update environment variables
update_dates_dict.txt:
	@echo "Cleaning old data"
	make clean

	@echo "Creating database if not in existance..."
	python pymlb/database_configuration.py

	@echo "Checking Databases for date ranges"
	python pymlb/util/get_update_config.py

	@echo "Environemnt variables set"
	@echo "Call make update to update tables"


create_database:
	python pymlb/database_configuration.py


update_data: update_dates_dict.txt .pitchfx .gameoutcomes .gamelogs data/external/mlb_id_retro_map.csv
	@echo "Full Pipe Successfully updated"


.pitchfx:
	@echo "Downloading pitch fx data"
	python pymlb/download_pfx.py
	touch .pitchfx


.gameoutcomes:
	@echo "Downloading game outcomes"
	python pymlb/get_game_outcomes.py
	touch .gameoutcomes


.gamelogs:
	@echo "Downloading Game Logs"
	python pymlb/download_game_logs.py
	touch .gamelogs


data/external/mlb_id_retro_map.csv:
	@echo Uploading player ids to database
	python pymlb/playerid_mapping_to_database.py


data/pitch_clean.pq: pymlb/get_pitches.py
	@echo "Pulling pitch data..."
	python $<


data/x_pitch.npy data/y_pitch_atbat_outcomes.npy data/y_pitch_locations.npy data/y_pitch_contact_types.npy: pymlb/get_pitches_train_test.py
	@echo "Creating train/test for pitches..."
	mkdir -p data/index_maps/
	python $<


data/train/y_pitch_atbat_outcomes_train.npy data/train/y_pitch_locations_train.npy data/train/y_pitch_contact_types_train.npy data/train/x_pitch_train.npy data/test/y_pitch_atbat_outcomes_test.npy data/test/y_pitch_locations.npy_test data/test/y_pitch_contact_types_test.npy data/test/x_pitch_test.npy: get_pitches_train_test.py
	@echo "Creating train/test for pitches..."
	mkdir -p data/index_maps/
	mkdir -p data/train/
	mkdir -p data/test/
	python $<


make train_test: data/train/y_pitch_atbat_outcomes_train.npy data/train/y_pitch_locations_train.npy data/train/y_pitch_contact_types_train.npy data/train/x_pitch_train.npy data/test/y_pitch_atbat_outcomes_test.npy data/test/y_pitch_locations.npy_test data/test/y_pitch_contact_types_test.npy data/test/x_pitch_test.npy
	@echo "SUCCESS"


# Cleans all processed data (not data downloads)
clean:
	rm -f update_dates_dict.txt
	rm -f .pitchfx
	rm -f .gamelogs
	rm -f data/external/mlb_id_retro_map.csv
